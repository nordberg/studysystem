({
	appDir: "./static/scripts",
	baseUrl: '.',
	dir: "./dist",
	modules: [{
		name: "main"
	}],
	findNestedDependencies: true,
	removeCombined: true,
	mainConfigFile: "./static/scripts/main.js",
})