'use strict';
var config = require('../config'),
	Steppy = require('twostep').Steppy,
	MongoClient = require('mongodb').MongoClient;

exports.up = function(next){
	Steppy(
		function() {
			MongoClient.connect(config.database.path, this.slot());
		},
		function(err, db) {
			var courses = db.collection('courses');
			this.pass(courses);
			courses.find().toArray(this.slot());
		},
		function(err, courses, items) {
			if (items.length) {
				items = items.map(function(item) {
					item.sections = item.sections.map(function(section) {
						var counts = {
							'lab': 1,
							'lection': 1,
							'practice': 1
						};
						section.subsections = section.subsections.map(function(ss) {
							if (counts[ss.type]) {
								ss.order = counts[ss.type]++;
							} else {
								ss.order = 0;
							}
							return ss;
						});
						return section;
					});
					return item;
				});

				var group = this.makeGroup();

				items.forEach(function(item, inx) {
					courses.save(item, group.slot());
				});
			} else {
				this.pass(true);
			}
		},
		function(err) {
			if (!err) return next();

			console.error('Error in orderedSubsections migration', err);
		}
	);
};

exports.down = function(next){
	next();
};
