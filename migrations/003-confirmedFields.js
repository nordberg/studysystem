'use strict';
var config = require('../config'),
	Steppy = require('twostep').Steppy,
	MongoClient = require('mongodb').MongoClient;

exports.up = function(next){
	Steppy(
		function() {
			MongoClient.connect(config.database.path, this.slot());
		},
		function(err, db) {
			var documents = db.collection('documents');
			this.pass(documents);
			documents.find().toArray(this.slot());
		},
		function(err, documents, items) {
			if (items.length) {
				items = items.map(function(item) {
					item.versions = item.versions.map(function(version) {
						if (version.confirmed) {
							version.state = 'confirmed';
						} else {
							version.state = 'undefined';
						}
						version.stateInfo = version.confirmationInfo;

						delete version.confirmed;
						delete version.confirmationInfo;

						return version;
					});
					return item;
				});

				var group = this.makeGroup();

				items.forEach(function(item, inx) {
					documents.save(item, group.slot());
				});
			} else {
				this.pass(true);
			}
		},
		function(err) {
			if (!err) return next();

			console.log('Error in confirmedFields migration', err);
		}
	);
};

exports.down = function(next){
	next();
};
