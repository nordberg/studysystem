'use strict';
var config = require('../config'),
	exec = require('child_process').exec,
	fs = require('fs'),
	path = require('path'),
	http = require('http'),
	TwoStep = require('twostep'),
	MongoClient = require('mongodb').MongoClient;

TwoStep(
    TwoStep.simple(function() {
		MongoClient.connect(config.database.path, this.slot());
	}),
	TwoStep.throwIfError(function(err, db) {
		db.dropDatabase(this.slot());
	}),
	TwoStep.throwIfError(function(err) {
		fs.readdir(config.documents.path, this.slot());
	}),
	TwoStep.simple(function(err, files) {
		if (err) {
			fs.mkdirSync(config.documents.path);
		}
		files = files || [];
		files.forEach(function(file) {
			fs.unlinkSync(path.join(config.documents.path, file));
		});
		this.pass(true);
	}),
	TwoStep.throwIfError(function(err, result) {
		fs.rmdir(config.documents.path, this.slot());
	}),
	TwoStep.throwIfError(function(err, result) {
		http.get(config.testDumpURL, this.slot());
	}),
	TwoStep.simple(function(response) {
		var name = path.join('./', Math.random().toString(36).substring(7));
		var file = fs.createWriteStream(name);
		response.pipe(file);
		this.pass(name, file);
		file.on('finish', this.slot());
	}),
	TwoStep.throwIfError(function(err, name, file) {
		file.close();
		this.pass(name)
	}),
	TwoStep.throwIfError(function(err, name) {
		this.pass(name)
		exec('tar -xzf ' + name, this.slot());
	}),
	TwoStep.throwIfError(function(err, name, stderr, stdout) {
		fs.unlinkSync(path.join('./', name));
		exec('mongorestore --db ' + config.dbName + ' ./studysystem', this.slot());
	}),
	TwoStep.throwIfError(function(err) {
		// I think it's will be better by fs, but...
		exec('rm -r ./studysystem', this.slot())
	}),
	TwoStep.simple(function(err) {
		if (err) {
			console.log(err)
		} else {
			process.exit(0);
		}
	})
);