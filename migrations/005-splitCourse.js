'use strict';
var config = require('../config'),
	Steppy = require('twostep').Steppy,
	MongoClient = require('mongodb').MongoClient;

exports.up = function(next){
	Steppy(
		function() {
			MongoClient.connect(config.database.path, this.slot());
		},
		function(err, db) {
			var courses = db.collection('courses'),
				sections = db.collection('sections'),
				subsections = db.collection('subsections'),
				sequences = db.collection('sequences');
			this.pass(courses, sections, subsections, sequences);
			courses.find().toArray(this.slot());
		},
		function(err, courses, sections, subsections, sequences, courseItems) {
			if (courseItems.length) {
				var sectionsCounter = 0,
					subsectionsCounter = 0,
					sectionItems = [],
					subsectionItems = [];

				courseItems = courseItems.map(function(item) {
					item.sections_ids = item.sections.map(function(section) {
						section.subsections_ids = section.subsections.map(function(subsection) {
							subsection.deleted = false;
							subsection.id = ++subsectionsCounter;
							subsectionItems.push(subsection);
							return subsection.id;
						});

						delete section.subsections;

						section.id = ++sectionsCounter;
						section.deleted = false;
						sectionItems.push(section);
						return section.id;
					});

					delete item.sections;

					return item;
				});

				var group = this.makeGroup();

				courseItems.forEach(function(item, inx) {
					courses.save(item, group.slot());
				});

				sectionItems.forEach(function(item, inx) {
					sections.save(item, group.slot());
				});

				subsectionItems.forEach(function(item, inx) {
					subsections.save(item, group.slot());
				});

				sequences.insert({collection: "sections", counter: sectionsCounter}, group.slot());
				sequences.insert({collection: "subsections", counter: subsectionsCounter}, group.slot());
			} else {
				this.pass(true);
			}
		},
		function(err) {
			if (!err) return next();

			console.error('Error in splitCourse migration', err);
		}
	);
};

exports.down = function(next){
	next();
};
