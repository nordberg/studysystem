'use strict';
var config = require('../config'),
	Steppy = require('twostep').Steppy,
	MongoClient = require('mongodb').MongoClient;

exports.up = function(next){
	Steppy(
		function() {
			MongoClient.connect(config.database.path, this.slot());
		},
		function(err, db) {
			var courses = db.collection('courses');
			this.pass(courses);
			courses.find().toArray(this.slot());
		},
		function(err, courses, items) {
			if (items.length) {
				items = items.map(function(item) {
					item.published = true;
					item.sections = item.sections.map(function(section) {
						section.subsections = section.subsections.map(function(ss) {
							ss.published = true;
							return ss;
						});
						section.published = true;
						return section;
					});
					return item;
				});

				var group = this.makeGroup();

				items.forEach(function(item, inx) {
					courses.save(item, group.slot());
				});
			} else {
				this.pass(true);
			}
		},
		function(err) {
			if (!err) return next();

			console.log('Error in addPublishedField migration', err);
		}
	);
};

exports.down = function(next){
	next();
};
