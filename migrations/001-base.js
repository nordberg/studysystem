'use strict';

var config = require('../config'),
	Steppy = require('twostep').Steppy,
	UserModel = require('../models').UserModel,
	GroupModel = require('../models').GroupModel,
	DocumentModel = require('../models').DocumentModel,
	MongoClient = require('mongodb').MongoClient;

exports.up = function(next){
	Steppy(
		function() {
			GroupModel.findOne({ name: 'admin' }, this.slot());
		},
		function(err, admin) {
			if (!admin) {
				var data = {
					name: 'admin',
					title: 'Администратор',
					description: ''
				};
				GroupModel.insert(data, this.slot());
			} else {
				this.pass(admin);
			}
		},
		function(err, admin) {
			this.pass(admin);
			GroupModel.findOne({ name: 'student' }, this.slot());
		},
		function(err, admin, student) {
			this.pass(admin);
			if (!student) {
				var data = {
					name: 'student',
					title: 'Студент',
					description: ''
				};
				GroupModel.insert(data, this.slot());
			} else {
				this.pass(student);
			}
		},
		function(err, admin, student) {
			this.pass(admin, student);
			GroupModel.findOne({ name: 'professor' }, this.slot());
		},
		function(err, admin, student, professor) {
			this.pass(admin, student);
			if (!professor) {
				var data = {
					name: 'professor',
					title: 'Преподаватель',
					description: ''
				};
				GroupModel.insert(data, this.slot());
			} else {
				this.pass(professor);
			}
		},
		function(err, adminGroup, studenGroup, professorGroup) {
			this.pass(adminGroup, studenGroup, professorGroup);
			UserModel.findOne(
				{ email: 'admin@learn-differently.ru' },
				this.slot()
			);
		},
		function(err, adminGroup, studenGroup, professorGroup, admin) {
			if (!admin) {
				UserModel.insert({
					firstName: 'Иван',
					lastName: 'Иванов',
					email: 'admin@learn-differently.ru',
					password: 'asdf1234',
					groups_ids: [adminGroup.id]
				}, this.slot());
			} else {
				this.pass(admin);
			}
		},
		function(err) {
			if (!err) return next();

			console.log('Error in base migration', err);
		}
	);
};

exports.down = function(next){
	next();
};
