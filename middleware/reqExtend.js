'use strict';

module.exports = function(req, res, next) {
	req.checkAuthorize = function(err, callback) {
		if (err) return callback(err);

		if (!req.user) {
			res.notAuthorized();
		} else {
			callback();
		}
	};
	next();
};