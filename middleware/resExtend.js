'use strict';

var codes = {
	UnhandledException: 127,
	InvalidUser: 2,
	UserNotAuthorized: 3,
	ValidationError: 4,
	AlreadyExist: 5,
	SessionExpired: 6,
	InvalidParams: 7
};

module.exports = function(req, res, next){
	res.validationError = function(message) {
		res.json(400, {
			message: message || 'Validation error',
			code: codes.ValidationError
		});
	};
	res.forbidden = function(message) {
		res.json(403, {
			message: message || 'Requrest forbidden',
			code: codes.ValidationError
		});
	};
	res.notFound = function(message) {
		res.json(404, {
			message: message || 'Not found',
			code: codes.ValidationError
		});
	};
	res.alreadyExist = function(message) {
		res.json(422, {
			message: message || 'Already Exist',
			code: codes.AlreadyExist
		});
	};
	res.invalidParams = function(message) {
		res.json(422, {
			message: message || 'Invalid params',
			code: codes.ValidationError
		});
	}
	res.notAllowed = function(message) {
		res.json(405, {
			message: message || 'Method not allowed',
			code: codes.UserNotAuthorized
		});
	};
	res.notAuthorized = function(message) {
		res.json(401, {
			message: message || 'User not authorized',
			code: codes.UserNotAuthorized
		});
	};
	res.sessionExpired = function(message) {
		res.json(401, {
			message: message || 'Session expired',
			code: codes.SessionExpired
		});
	};
	res.onError = function(err) {
		console.log(err.stack);
		switch (err.name) {
			case 'ValidationError': {
				res.json(500, {
					status: codes.ValidationError
				});
			} break;

			default: {
				res.json(500, {
					status: codes.UnhandledException
				});
			}
		}
	};
	res.ok = function(data) {
		res.json(200, data);
	};
	res.created = function(data) {
		res.json(201, data);
	};
	res.statused = function(status) {
		status = status || 127;
		res.json({
			status: status
		});
	};
	next();
};