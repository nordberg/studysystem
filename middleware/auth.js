'use strict';
var SessionModel = require('../models').SessionModel,
	UserModel = require('../models').UserModel,
	GroupModel = require('../models').GroupModel,
	_ = require('underscore'),
	Steppy = require('twostep').Steppy;

module.exports = function(req, res, next) {
	Steppy(
		function() {
			var token = req.headers['authorization-bearer'] || req.query.token;
			if (token) {
				delete req.query.token;
				req.session.get(token, this.slot());
			} else {
				next();
			}
		},
		function(err, session) {
			if (session) {
				this.pass(session);
				req.session.isExpired(session.token, this.slot());
			} else {
				res.notAuthorized();
			}
		},
		function(err, session, isExpired) {
			if (!isExpired) {
				this.pass(session);
				UserModel.findOne({ id: session.userId } ,this.slot());
			} else {
				res.sessionExpired();
			}
		},
		function(err, session, user) {
			this.pass(session);
			this.pass(user);
			GroupModel.find({ id: { $in: user.groups_ids } }, this.slot());
		},
		function(err, session, user, groups) {
			if (!user) {
				res.notFound();
			} else {
				user.groups = groups;

				user.isAdmin = _(user.groups).some(function(group) {
					return group.name == 'admin';
				});

				req.user = user;
				req.token = session.token;
			}
			next();
		},
		res.onError
	);
};
