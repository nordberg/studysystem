'use strict';

var MongoClient = new require('mongodb').MongoClient,
	uuid = require('node-uuid');

module.exports = function(params) {
	var db,
		oneDay = 8640000,
		sequencesCollection = 'sequences';

	params = params || {};

	var generateToken = function() {
		return uuid.v1();
	};

	var SessionManager = function() {
		var self = this;
		MongoClient.connect('mongodb://127.0.0.1:27017/studysystem', function(err, connection) {
			self.db = connection;
			self.collection = connection.collection(params.collection || 'sessions');
		});
	};

	SessionManager.prototype.get = function(token, callback) {
		this.getBy({ token: token }, callback);
	};

	SessionManager.prototype.getBy = function(criteria, callback) {
		this.collection.findOne(criteria, {_id: 0}, function(err, data) {
			if (err) {
				callback(err);
			} else {
				callback(null, data);
			}
		});
	};

	SessionManager.prototype.create = function(callback) {
		var token = generateToken();
		this.collection.save({ token: token }, function(err, data) {
			if (err) {
				callback(err);
			} else {
				delete data._id;
				callback(null, data);
			}
		});
	};

	SessionManager.prototype.set = function(token, data, callback) {
		data.ttl = (new Date()).getTime() + (params.maxTime || oneDay);
		data.token = token;
		this.collection.findAndModify(
			{token: token}, [], data, {new: true, upsert: true},
			function(err, data) {
				if (err) {
					callback(err);
				} else {
					callback(null, data);
				}
			}
		);
	};

	SessionManager.prototype.remove = function(token, callback) {
		this.collection.remove({token: token}, {single: true}, callback);
	};

	SessionManager.prototype.isExpired = function(token, callback) {
		this.collection.findOne({token: token}, function(err, session) {
			if (err) {
				callback(err);
			} else {
				if (session) {
					var now = (new Date()).getTime();
					if (now > session.ttl) {
						callback(null, true);
					} else {
						callback(null, false);
					}
				} else {
					callback(new Error('Token doesn\'t exist'));
				}
			}
		});
	};

	var sessionManager = new SessionManager();

	return function(req, res, next) {
		req.session = sessionManager;
		next();
	};
};