'use strict';

define(['ember', 'ember-model'], function(Ember) {
	return Ember.RESTAdapter.extend({
		additionalAction: function(record, action) {
			var primaryKey = Ember.get(record.constructor, 'primaryKey'),
				url = this.buildURL(
					record.constructor, Ember.get(record, primaryKey)
				),
				self = this;

			return this.ajax(url, {action: action}, 'POST').then(function(dataToLoad) {
				var data = Ember.get(dataToLoad, record.constructor.rootKey);
				record.load(data[primaryKey], data);
				self.didAddititonalAction(record, data[primaryKey], action);
				return record;
			});
		},
		didAddititonalAction: function(record, data, action) {

		}
	});
});