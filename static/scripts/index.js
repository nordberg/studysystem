'use strict';

define([
	'authManager',
	'views/index', 'controllers/index',
	'routes/index', 'models/index',
	'components/index',
	'utils/errors',
	'jquery'
], function(
	AuthManager, views, controllers, routes, models, components, errors, $
) {
	return $.extend({
		AuthManager: AuthManager
	}, components, views, controllers, routes, models, errors);
});