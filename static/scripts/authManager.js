'use strict';

define([
	'ember', 'models/user', 'jquery'
], function(Ember, User, $) {
	var ApiKey = Ember.Object.extend({
		accessToken: ''
	});

	var AuthManager = Ember.Object.extend({
		init: function() {
			this._super();
			//TODO Cookie path doesn't work and i place it hear. We have to think more.
			var accessToken = localStorage.getItem('accessToken'),
				userId = localStorage.getItem('userId');
			if (!Ember.isEmpty(accessToken) && !Ember.isEmpty(userId)) {
				this.authenticate(accessToken, userId);
			}
		},

		isAuthenticated: function() {
			return !Ember.isEmpty(this.get('apiKey.accessToken'));
		},

		authenticate: function(accessToken, userId) {
			Ember.$.ajaxSetup({
				headers: {
					'Authorization-Bearer': accessToken
				}
			});
			this.set('apiKey', ApiKey.create({
				accessToken: accessToken,
			}));
			var self = this;
			User.fetch(userId).then(function(user) {
				self.set('user', user);
			});
		},

		reset: function() {
			Ember.$.ajaxSetup({
				headers: {
					'Authorization-Bearer': ''
				}
			});
			this.set('apiKey', '');
			this.set('user', null);
		},

		apiKeyObserver: function() {
			if (Ember.isEmpty(this.get('apiKey'))) {
				localStorage.removeItem('accessToken');
			} else {
				localStorage.setItem('accessToken', this.get('apiKey.accessToken'));
			}
		}.observes('apiKey'),

		userObserver: function() {
			if (Ember.isEmpty(this.get('user'))) {
				localStorage.removeItem('userId');
			} else {
				localStorage.setItem('userId', this.get('user.id'));
			}
		}.observes('user')
	});
	return AuthManager;
});