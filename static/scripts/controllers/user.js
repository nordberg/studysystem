'use strict';

define(['ember', 'utils/alert'], function(Ember, Alert) {
	var UserController = Ember.ObjectController.extend({
		isMe: function() {
			var authManager = this.get('authManager');
			return this.get('authManager').get('user.id') == this.get('model.id');
		}.property('authManager.user'),

		actions: {
			save: function() {
				this.get('model').save().then(function() {
					Alert.show('success', 'Изменения сохранены');
				}, function() {
					Alert.show('danger', 'Ошибка сохранения');
				});
			}
		}
	});

	return UserController;
});
