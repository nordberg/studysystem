'use strict';

define(['ember', 'utils/jsonRequest'], function(Ember, jsonRequest) {
	var LoginController = Ember.ObjectController.extend({
		loginFailed: false,

		resetErrors: function() {
			this.set('loginFailed', false);
		}.observes('email', 'password'),

		actions: {
			login: function() {
				var params = this.get('model'),
					self = this;
				Ember.run(function() {
					jsonRequest('/login', params).then(function(data) {
						if (data.token && data.user) {
							Ember.run(
								self.get('authManager'),
								'authenticate',
								data.token,
								data.user
							);
							Ember.run(self, 'transitionToRoute', 'courses');
						} else {
							Ember.run(self, 'set', 'loginFailed', true);
						}
					}, function() {
						Ember.run(self, 'set', 'loginFailed', true);
					});
				});
			}
		}
	});
	return LoginController;
});