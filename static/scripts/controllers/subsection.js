 'use strict';

define(['ember', 'utils/transform'], function(Ember, Transformer) {
	var CourseSectionSubsectionController = Ember.Controller.extend({
		value: function() {
			return Transformer.mdToHtml(this.get('model.markdown'));
		}.property('markdown'),
		canEdit: function() {
			return this.get('authManager.user.isProfessor');	
		}.property('authManager.user')
	});

	return CourseSectionSubsectionController;
});
