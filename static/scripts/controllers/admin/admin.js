'use strict';

define(['ember', 'jquery'], function(Ember, $) {
	var AdminController = Ember.Controller.extend({
		user: Ember.computed.alias('authManager.user')
	});
	return AdminController;
});