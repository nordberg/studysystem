'use strict';

define([
	'ember', 'models/user', 'utils/alert', 'utils/jsonRequest'
], function(
	Ember, UserModel, Alert, jsonRequest
) {
	var AdminUserController = Ember.Controller.extend({
		actions: {
			save: function() {
				this.get('user').save().then(function() {
					Alert.show('success', 'Изменения сохранены');
				}, function() {
					Alert.show('danger', 'Ошибка при сохранении');
				});
			},

			register: function() {
				jsonRequest('/register', this.get('user').toJSON()).then(function(result) {
					UserModel.load(result.user);
					Alert.show('success', 'Пользователь успешно зарегестрирован');
				}, function() {
					Alert.show('danger', 'Ошибка при регистрации');
				});
			},

			generatePassword: function() {
				this.get('user').set(
					'password', Math.random().toString(36).substring(7)
				);
			},

			back: function() {
				Alert.hide();
				this.transitionToRoute('admin.users.index');
			}
		}
	});
	return AdminUserController;
});

