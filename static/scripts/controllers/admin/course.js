'use strict';

define([
	'ember', 'jquery', 'utils/alert', 'models/course', 'models/user', 'models/document'
], function(
	Ember, $, Alert, Course, UserModel, DocumentModel
) {
	var AdminCourseController = Ember.Controller.extend({
		init: function() {
			this._super();
			this.set('docsToSave', []);
			this.set('modelsToRemove', []);
		},

		actions: {
			addSection: function() {
				var section = this.get('model').get('sections').create({});
			},
			removeSection: function(section) {
				if (section.get('subsections.length')) {
					this.send('openModal', 'modal.info', {
						message: 'Невозможно удалить раздел, содержащий подразделы.',
						buttonTitle: 'Ок'
					});
				} else {
					this.get('course.sections').removeObject(section);
				}
			},
			addMaterial: function() {
				this.get('model.materials').create({
					isNew: false
				});
			},
			removeMaterial: function(material) {
				this.get('model.materials').removeObject(material);
			},
			addSubsection: function(section) {
				console.log(section)
				section.get('subsections').create({});
			},
			removeSubsection: function(section, subsection) {
				if (subsection.content) {
					modelsToRemove.pushObject(subsection.content);
				}
				section.get('subsections').removeObject(subsection);
			},
			save: function() {
				var self = this;
				var course = self.get('model');
				var docsPromises = this.get('docsToSave').map(function(doc) {
					return doc.save();
				});
				Ember.RSVP.all(docsPromises).then(function() {
					course.save().then(
						function(loadedCourse) {
							Alert.show('success', 'Изменения сохранены');
							self.transitionToRoute(
								'admin.courses.edit', loadedCourse.get('id')
							);
						}, function() {
							Alert.show('danger', 'Ошибка при сохранении');
						}
					);
				});
			},
			createSubsectionContent: function(subsection) {
				var doc = DocumentModel.create({
					title: subsection.get('title')
				});
				doc.get('authors').pushObject(this.get('authManager.user'));
				subsection.set('content', doc);
				this.get('docsToSave').pushObject(doc);
			},
			goToDocument: function(doc) {
				if (this.get('model.isDirty')) {
					this.send('openModal', 'modal.info', {
						message: 'Прежде чем перейти к редактированию документа необходимо сохранить сделанные на странице изменения.',
						buttonTitle: 'Ок'
					});
				} else {
					this.transitionToRoute('admin.documents.edit', doc);
				}
			},
			back: function() {
				Alert.hide();
				this.transitionToRoute('admin.courses');
			},
			publish: function(item) {
				if (!item.get('published')) {
					item.set('published', true);
					this.get('model').save();
				}
			},
			unpublish: function(item) {
				if (item.get('published')) {
					item.set('published', false);
					this.get('model').save();
				}
			}
		}
	});

	return AdminCourseController;
});
