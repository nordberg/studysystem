'use strict';

define([
	'ember', 'jquery', 'utils/alert'
], function(
	Ember, $, Alert
) {
	var AdminGroupController = Ember.Controller.extend({
		actions: {
			save: function() {
				var self = this;
				this.get('group').save().then(function(group) {
					self.transitionToRoute('admin.groups.edit', group);
					Alert.show('success', 'Изменения сохранены');
				}, function() {
					Alert.show('danger', 'Ошибка сохранения');
				});
			},
			back: function() {
				Alert.hide();
				this.transitionToRoute('admin.groups');
			}
		}
	});

	return AdminGroupController;
});

