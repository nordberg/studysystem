'use strict';

define(['ember', 'marked', 'utils/alert'], function(Ember, marked, alert) {
	var AdminDocumentsEditController = Ember.ObjectController.extend({
		actualVersion: null,

		actions: {
			saveVersion: function(value) {
				var self = this;
				this.get('model').createVersion(value).then(function(version) {
					if (version.get('state') === 'confirmed') {
						alert.show('success', 'Изменения успешно сохранены');
					} else {
						alert.show('success', 'Изменения сохранены и ожидают подтверждение')
					}
					self.set('actualVersion', version);
				});
			},
			updateContent: function(content) {
				this.get('model').set('content', content);
				this.set('actualVersion', null);
			},
			selectVersion: function(version) {
				var self = this;
				this.get('model').loadContentForVersion(version).then(function() {
					self.set('actualVersion', version);
				});
			},
			confirmVersion: function(version) {
				this.get('model').confirmVersion(version).then(function(result) {
					console.log('version confirmed', result);
				});
			},
			rejectVersion: function(version) {
				this.get('model').rejectVersion(version).then(function(result) {
					console.log('version rejected', result);
				});
			},
			loadActualVersion: function() {
				this.set('actualVersion', null)
				this.send('selectVersion', this.get('model').get('lastVersion'));
			},
			showSidebar: function() {
				this.set('sidebarPopupVisible', true);
			},
			showAuthors: function() {
				this.set('sidebarPopupVisible', !this.get('sidebarPopupVisible'));
			},
			showTutorial: function() {
				this.send('openModal', 'modal.info', {
					message: 'p # Заголовок первого уровняp p ## Заголовок второго уровня'
				});
			},
			goBack: function() {
				window.history.back();
			}
		}
	});
	return AdminDocumentsEditController;
});