'use strict';

define([
	'controllers/modalInfo',

	'controllers/auth/login',

	'controllers/course',
	'controllers/section',
	'controllers/subsection',

	'controllers/users',
	'controllers/user',

	'controllers/documents/edit',

	'controllers/admin/admin',
	'controllers/admin/course',
	'controllers/admin/group',
	'controllers/admin/user',
	'controllers/alerts'
], function(
	ModalInfoController,
	LoginController,

	Course,
	Section,
	Subsection,

	UsersController,
	UserController,

	DocumentsEditController,

	AdminController,
	AdminCourseController,
	AdminGroupController,
	AdminUserController,
	AlertsController
) {
	//TODO make placeholder in deps and put all files from folders to id
	//after it iterate over all arguments and make dictionary
	return {
		ModalInfoController: ModalInfoController,

		LoginController: LoginController,

		CourseIndexController: Course,
		CourseSectionIndexController: Section,
		CourseSectionSubsectionIndexController: Subsection,

		UsersController: UsersController,
		UsersUserController: UserController,

		DocumentsEditController: DocumentsEditController,

		AdminController: AdminController,
		AdminCourseController: AdminCourseController,
		AdminGroupController: AdminGroupController,
		AdminUserController: AdminUserController,
		AlertsController: AlertsController
	};
});
