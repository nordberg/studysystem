'use strict';

define([
	'ember', 'jquery', 'utils/alert'
], function(
	Ember, $, alert
) {
	var AlertsController = Ember.Controller.extend({
		afterSaving: false,
		init: function() {
			console.log('alerts controller initialized');
		},
		saveObserver: function() {

			if (this.get('afterSaving')) {
				alert.show('success', 'Изменения сохранены');
			}

			if (this.get('content').get('isSaving')) {
				this.set('afterSaving', true);
				// can we add loading animation, may be?

			} else {
				this.set('afterSaving', false);
			}

		}.observes('content.isSaving'),
		test: function() {
			console.log(123);
		}
	});
	return AlertsController;
});
