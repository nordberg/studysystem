'use strict';

define(['ember'], function(Ember) {
	var ModalInfoController = Ember.ObjectController.extend({
		actions: {
			close: function() {
				return this.send('closeModal');
			}
		}
	});
	return ModalInfoController;
});