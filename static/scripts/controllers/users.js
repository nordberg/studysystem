'use strict';

define(['ember'], function(Ember) {
	var UsersController =  Ember.Controller.extend({
		user: Ember.computed.alias('authManager.user'),
		actions: {
			edit: function() {
				this.get('model')
			}
		}
	});
	return UsersController;
});
