'use strict';

define([
	'ember'
], function(
	Ember
) {
	var CourseController = Ember.Controller.extend({
		isEditingTitle: false,
		isEditingDescription: false,
		isAddingSection: false,
		init: function() {
			this._super();
		},
		canEdit: function() {
			if (this.get('authManager.user.isAdmin')) {
				return true;
			}

			if (this.get('authManager.user.isProfessor')) {
				var myId = this.get('authManager.user.id');
				return this.get('course.authors').some(function(author) {
					return author.get('id') == myId;
				});
			}
		}.property(
			'authManager.user.isAdmin',
			'authManager.user.isProfessor',
			'authManager.user.id',
			'course.authors'
		),
		actions: {
			editTitle: function() {
				this.set('isEditingTitle', true);
			},
			editDescription: function() {
				this.set('isEditingDescription', true);
			},
			focusOutAction: function() {
				this.set('isEditingTitle', false);
				this.set('isEditingDescription', false);
				this.send('save');
			},
			showFormAddSection: function() {
				this.set('isAddingSection', true);
			},
			addSection: function() {
				this.set('isAddingSection', false);
				var section = this.get('model').get('sections').create({
					title: this.get('newSectionTitle'),
					alias: this.get('newSectionAlias'),
					description: this.get('newSectionDescription'),
					duration: this.get('newSectionDuration')
				});
				this.send('save');
			},
			save: function() {
				this.get('model').save();
			}
		}
	});

	return CourseController;
});
