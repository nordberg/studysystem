'use strict';

define([
	'ember', 'models/course', 'models/document', 'utils/alert'
], function(
	Ember, CourseModel, DocumentModel, Alert
) {
	var CourseSectionController = Ember.Controller.extend({
		isEditingTitle: false,
		isAddingSubsection: false,
		init: function() {
			this.send('resetNewSubsection');
			this.set('docsToSave', []);
		},
		actions: {
			setPublished: function(published) {
				this.set('section.published', published)
				this.send('save');
			},
			editTitle: function() {
				this.set('isEditingTitle', true);
			},
			focusOutAction: function() {
				this.set('isEditingTitle', false);
				this.send('save');
			},
			showFormAddSubsection: function() {
				this.set('isAddingSubsection', true);
			},
			addSubsection: function () {
				var self = this,
					newSubsection = this.get('newSubsection'),
					doc = DocumentModel.create({
						title: this.get('newSubsection.title')
					});

				doc.get('authors').pushObject(this.get('authManager.user'));
				newSubsection.set('content', doc);
				this.get('section.subsections').pushObject(newSubsection);
				this.send('resetNewSubsection');

				doc.save().then(function() {
					self.get('course').save().then(function() {
						self.set('isAddingSubsection', false);
					});
				});
			},
			cancelAddingSubsection: function() {
				this.set('isAddingSubsection', false);
				this.send('resetNewSubsection');
			},
			save: function() {
				this.get('course').save();
			},
			resetNewSubsection: function() {
				this.set(
					'newSubsection',
					CourseModel.create({}).get('sections').create({}).get('subsections').create({})
				);
			},
			goToDocument: function(doc) {
				if (this.get('course.isDirty')) {
					this.send('openModal', 'modal.info', {
						message: 'Прежде чем перейти к редактированию документа необходимо сохранить сделанные на странице изменения.',
						buttonTitle: 'Ок'
					});
				} else {
					this.transitionToRoute('admin.documents.edit', doc);
				}
			},
		}
	});

	return CourseSectionController;
});
