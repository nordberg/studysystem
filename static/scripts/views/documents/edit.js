'use strict';

define([
	'ember', 'ace/ace', 'marked', 'utils/transform', 'jquery'
], function(Ember, ace, marked, Transform, $) {
	var versionChanged = false;

	var DocumentEditorView = Ember.View.extend({
		templateName: 'views/documents/edit',

		editSelector: '#document__editor',
		previewSelector: '.document__preview',
		headerSelector: '.document__header',
		layoutSelector: '.document-l',

		previewElement: null,
		versionsPopupVisible: false,

		rawChanged: function() {
			if (!versionChanged) {
				var editorValue = this.get('editor').getValue();
				this.get('controller').send('updateContent', editorValue);
				this.set('unsavedContent', editorValue);
			} else {
				versionChanged = false;
			}
			this.get('previewElement').html(
				Transform.mdToHtml(this.get('doc').get('content'))
			);
		},

		setEditorValue: function(value) {
			var editor = this.get('editor');
			editor.setValue(value)
			editor.clearSelection();
			editor.focus();
			editor.moveCursorTo(0, 0);
		},

		actualVersionObserver: function() {
			if (this.get('controller.actualVersion')) {
				versionChanged = true;
				this.setEditorValue(this.get('doc').get('content'));
			}
		}.observes('controller.actualVersion'),

		scrollContent: function(scrollPosition) {
			this.get('previewElement').scrollTop(scrollPosition);
		},

		scrollPreview: function() {
			this.get('editor').getSession().setScrollTop(
				this.get('previewElement').scrollTop()
			);
		},

		repositionElements: function() {
			var headerHeight = $(this.get('headerSelector')).outerHeight();
			var offset = $(this.get('layoutSelector')).offset();
			var height = $(window).height() - headerHeight - offset.top - 50;
			var width = $(this.get('layoutSelector')).width() - 40;

			$(this.get('editSelector')).height(height);
			$(this.get('editSelector')).width(width / 2);
			$(this.get('previewSelector')).height(height);
			$(this.get('previewSelector')).width(width / 2 );
			this.get('editor').resize();
			$(this.get('layoutSelector')).height(height);
		},

		didInsertElement: function() {
			var editor = ace.edit('document__editor');

			this.set('editor', editor);
			this.set(
				'previewElement',
				this.$().find('.document__preview')
			);

			editor.setTheme("ace/theme/textmate");
			editor.getSession().setMode("ace/mode/markdown");
			editor.getSession().setUseWrapMode(true);

			editor.getSession().on('changeScrollTop',
				this.scrollContent.bind(this)
			);
			this.get('previewElement').on('scroll',
				this.scrollPreview.bind(this)
			);

			editor.on('input', this.rawChanged.bind(this));
			this.get('controller').send('loadActualVersion');

			$(window).on('resize', this.repositionElements.bind(this));

			this.repositionElements();
		},

		actions: {
			loadUnsavedContent: function() {
				this.setEditorValue(this.get('unsavedContent'));
			},
			saveVersion: function(value) {
				this.set('unsavedContent', null);
				this.get('controller').send('saveVersion', value);
			},
			toggleVersionsPopup: function() {
				this.set('versionsPopupVisible', !this.get('versionsPopupVisible'));
			},
			doAction: function(action) {
				var wrapTextWith = function(editor, text) {
					var isEmptySelection = editor.getSelection().isEmpty();
					var range = editor.getSelectionRange();
					var length = text.length;
					editor.clearSelection();
					editor.moveCursorToPosition(range.start);
					editor.insert(text);
					range.end.column = range.end.column + length;
					editor.moveCursorToPosition(range.end);
					editor.insert(text);
					if (isEmptySelection) {
						editor.moveCursorToPosition(range.end);
						editor.focus();
					}
				};

				var editor = this.get('editor');

				switch (action) {
					case 'undo':
						editor.getSession().getUndoManager().undo();
						editor.clearSelection();
					break;
					case 'redo':
						editor.getSession().getUndoManager().redo();
						editor.clearSelection();
					break;
					case 'makeBold':
						wrapTextWith(editor, '**');
					break;
					case 'makeItalic':
						wrapTextWith(editor, '_');
					break;
				}
			}
		}
	});
	return DocumentEditorView;
});
