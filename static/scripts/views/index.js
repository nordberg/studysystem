'use strict';

define([
	'views/application',
	'views/courses/subsectionBadge',
	'views/courses/subsectionItem',
	'views/courses/sectionItem',
	'views/documents/edit',
	'views/users/user',
	'views/alerts',
	'views/checkboxList',
	'views/tooltip'
], function(
	ApplicationView,
	SubsectionBadge,
	SubsectionItem,
	SectionItem,
	DocumentEditView,
	UsersUserView,
	Alerts,
	CheckboxListView,
	TooltipView
) {
	//TODO make placeholder in deps and put all files from folders to id
	//after it iterate over all arguments and make dictionary
	var FocusedTextField = Ember.TextField.extend({
		didInsertElement: function() {
			this.$().focus();	
		}
	});

	var FocusedTextArea = Ember.TextArea.extend({
		didInsertElement: function() {
			this.$().focus();	
		}
	});

	return {
		ApplicationView: ApplicationView,
		SubsectionBadgeView: SubsectionBadge,
		SubsectionItemView: SubsectionItem,
		SectionContentView: SectionItem,
		DocumentEditView: DocumentEditView,
		UsersUserView: UsersUserView,
		AlertsView: Alerts,
		CheckboxListView: CheckboxListView,
		TooltipView: TooltipView,
		FocusedTextField: FocusedTextField,
		FocusedTextArea: FocusedTextArea
	};
});
