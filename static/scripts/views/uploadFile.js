'use strict';

define(['ember', 'jquery.fileupload'], function(Ember, jQueryFileUpload) {
	var UploadFile = Ember.TextField.extend({
		tagName: 'input',
		attributeBindings: ['name'],
		type: 'file',
		didInsertElement: function() {
			// console.log(this)
			this._super();
			this.$().fileupload();
		}
		// change: function(e) {
		// 	var reader = new FileReader(),
		// 		self = this;
		// 	reader.onload = function(e) {
		// 		var fileToUpload = e.target.result;
		// 		Ember.run(function() {
		// 			console.log(reader.result)
		// 			$('#originalId').attr('src', fileToUpload)
		// 			self.set('file', fileToUpload);
		// 		});
		// 	};
		// 	console.dir(e.target)
		// 	return reader.readAsDataURL(e.target.files[0]);
		// }
	});

	return UploadFile;
});
