'use strict';

define(['ember'], function(Ember) {
	var UserView = Ember.View.extend({
		templateName: 'users/user',
		isEditing: false,
		isEditVisible: function() {
			return !this.get('isEditing');	
		}.property('isEditing'),
		actions: {
			edit: function() {
				this.set('isEditing', true);
			},
			save: function() {
				this.set('isEditing', false);
				this.get('controller').send('save')
			}
		}
	});

	return UserView;
});
