'use strict';

define(['ember'], function(Ember) {
	var ApplicationView = Ember.View.extend({
		classNames: ['ember-app-wrap']
	});

	return ApplicationView;
});