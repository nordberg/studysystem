define(['ember'], function(Ember) {
	var SectionItem = Ember.View.extend({
		templateName: 'views/courses/sectionView',
		didInsertElement: function() {
			var ItemProperties = function(top, minGap) {
					this.top = top || 0;
					this.count = 0;
					this.gap = 0;
					this.minGap = minGap || 0;
					this.height = 0;
				},
				height = this.$().outerHeight(),
				properties = {
					lab: new ItemProperties(10, 20),
					practice: new ItemProperties(10, 20)
				};

			this.get('childViews').forEach(function(childView) {
				var type = childView.get('type');

				if (properties[type]) {
					properties[type].count++;
					properties[type].height = childView.$().height();
				}
			});

			for (item in properties) {
				if (properties[item].count) {
					var gap = properties[item].minGap + properties[item].height,
						minHeight = gap * properties[item].count
						+ properties[item].minGap;

					if (minHeight > height) {
						minHeight -= parseInt(this.$().css('padding-top'))
						+ parseInt(this.$().css('padding-bottom'));

						this.$().height(minHeight)
						properties[item].gap = gap;
					} else {
						properties[item].gap = Math.floor(
							(height - properties[item].top) / properties[item].count
						);
					}
				}
			}

			this.get('childViews').forEach(function(childView) {
				var type = childView.get('type');

				if (properties[type]) {
					childView.$().css('top', properties[type].top)
					properties[type].top += properties[type].gap;
				}
			});
		}
	});

	return SectionItem;
});
