define(['ember'], function(Ember) {
	var SubsectionItem = Ember.View.extend({
		templateName: 'views/courses/subsectionItem',
		classNameBindings: [
			'typeClass',
			'modifierClass',
			'isCompletedClass'
		],
		linkTypes: {'lection' : true},

		type: function() {
			return this.get('subsection.type');
		}.property('subsection.type'),

		typeClass: function() {
			if (this.get('isLink')) {
				return 'subsection-link';
			} else {
				return 'subsection-pin';
			}
		}.property('subsection.type'),

		modifierClass: function() {
			return this.get('typeClass') + '_' + this.get('subsection.type')
		}.property('subsection.type'),

		isCompletedClass: function() {
			return this.get('modifierClass') + '_completed';
		}.property('modifierClass'),

		isLink: function() {
			return this.get('linkTypes')[this.get('subsection.type')];
		}.property('subsection.type'),

		mouseEnter: function() {
			if (!this.get('isLink')) {
				this.get('childViews')[0].show();
			}
		},

		mouseLeave: function() {
			if (!this.get('isLink')) {
				this.get('childViews')[0].hide();
			}
		}
	});

	return SubsectionItem;
});
