'use strict';

define(['ember'], function(Ember) {
	var SubsectionBadge = Ember.View.extend({
		classNames: ['badge'],
		classNameBindings: ['subsectionTypeClass'],

		templateName: 'views/courses/subsectionBadge',

		subsectionTypeClass: function() {
			var type = this.get('type'),
				resultClass = 'badge_blue';

			switch (type) {
				case 'lection':
					resultClass = 'badge_blue';
					break;
				case 'lab':
					resultClass = 'badge_green';
					break;
				case 'practice':
					resultClass = 'badge_sand';
					break;
				default:
					resultClass = 'badge_blue';
			}
			return resultClass;
		}.property('type')
	});

	return SubsectionBadge;
});