'use strict';

define(['ember'], function(Ember) {
	var CheckboxListView = Ember.View.extend({
		templateName: 'views/checkboxList',

		keyField: function() {
			return this.get('key') || 'id';
		}.property('key'),

		_getCheckedItemsHash: function() {
			var checkedItemsHash = {},
				keyField = this.get('keyField');

			this.get('checkedItems').forEach(function(item) {
				checkedItemsHash[item.get(keyField)] = item;
			});

			return checkedItemsHash;
		},

		items: function() {
			var allItems = this.get('allItems'),
				checkedItemsHash = this._getCheckedItemsHash(),
				keyField = this.get('keyField'),
				labelPath = this.get('labelPath');

			return this.get('allItems').map(function(item) {
				return {
					item: item,
					label: item.get(labelPath),
					checked: checkedItemsHash[item.get(keyField)] ? true : false
				};
			});
		}.property('allItems'),

		itemsCheckedObserver: function() {
			var viewCheckedItems = this.get('items').filterBy('checked').mapBy('item'),
				self = this;

			this.get('checkedItems').clear();

			viewCheckedItems.forEach(function(item) {
				self.get('checkedItems').pushObject(item);
			});
		}.observes('items.@each.checked')
	});

	return CheckboxListView;
})
