'use strict';

/** Simply alert messages.
 *	Use: just put in template such string with you own parameters
 *	"StudySystem.AlertsView type="<type>" message="<type>" isVisible=<flag>"
 *	Where <type> is type of your alert, can be 'success', 'info', 'warning' or 'danger';
 *	<message> is message, which you want to show;
 *	<flag> is boolean parameter (false -- hide alert, true -- show).
 *	But everything will be perfect if we can define view or component,
 *	which can be showed by one method, for exapmle, 'show' with three parameters.
 *	But main question is how can we use view method in controller.
 */

define(['ember'], function(Ember) {
	var Alerts = Ember.View.extend({
		types: {
			success: 'alert-success',
			info: 'alert-info',
			warning: 'alert-warning',
			danger: 'alert-danger'
		},
		classNames: ['alert', 'alert_top'],
		classNameBindings: ['currentTypeStyle'],

		isVisible: false,
		templateName: 'views/alert',

		currentType: '',
		currentTypeStyle: function() {
			return Ember.get(this.get('types'), this.get('currentType'));
		}.property('currentType'),

		message: '',

		show: function(type, message) {
			this.set('message', message);
			this.set('currentType', type);
			this.set('isVisible', true);

			var self = this;
			var currentTimeout = setTimeout(function() {
				self.$().fadeOut(1000, function() {
					self.set('isVisible', false);
					self.set('currentType', '');
					self.set('message', '');
				});
			}, 2000);
		},

		hide: function() {
			///TODO Must be implemented within the timeouts's constraints.
		}
	});

	return Alerts;
});