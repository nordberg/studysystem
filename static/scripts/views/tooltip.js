define(['ember'], function(Ember) {
	var TooltipView = Ember.View.extend({
		layoutName: 'views/tooltip',
		classNames: ['tooltip-wrap'],
		didInsertElement: function() {
			this.hide();
			var tooltip = this.$().find('.tooltip-wrap__tooltip'),
				type = this.get('type') || 'top';

			tooltip.addClass('tooltip-wrap__tooltip_' + type);

			switch (type) {
				case 'top':
					tooltip.css('top', -tooltip.outerHeight(true));
					break;
				case 'right':
					tooltip.css('right', -tooltip.outerWidth(true));
					break;
				case 'left':
					tooltip.css('left', -tooltip.outerWidth(true));
				default:
					console.error('Unknown tooltip type');
					break;
			}

		},
		show: function() {
			this.$().find('.tooltip-wrap__tooltip').fadeIn();
		},
		hide: function() {
			this.$().find('.tooltip-wrap__tooltip').hide();
		}
	});

	return TooltipView;
});
