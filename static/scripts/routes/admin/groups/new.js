'use strict';

define([
	'routes/authenticated', 'models/group'
], function(AuthenticatedRoute, GroupModel) {
	var AdminGroupsNewRoute = AuthenticatedRoute.extend({
		model: function(params) {
			return GroupModel.create();
		},
		setupController: function(controller, model) {
			this._super(controller, model);
			this.controllerFor('admin.group').setProperties({
				isNew: false,
				group: model
			});
		},
		renderTemplate: function() {
			this.render('admin.groups.form', {
				controller: 'admin.group'
			});
		}
	});
	
	return AdminGroupsNewRoute;
});
