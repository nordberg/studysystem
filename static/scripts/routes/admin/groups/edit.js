'use strict';

define([
	'routes/authenticated', 'models/group'
], function(AuthenticatedRoute, GroupModel) {
	var AdminGroupsEditRoute = AuthenticatedRoute.extend({
		model: function(params) {
			return GroupModel.fetch(params.group_id);
		},
		setupController: function(controller, model) {
			this._super(controller, model);
			this.controllerFor('admin.group').setProperties({
				isNew: false,
				group: model
			});
		},
		renderTemplate: function() {
			this.render('admin.groups.form', {
				controller: 'admin.group'
			});
		}
	});

	return AdminGroupsEditRoute;
});
