'use strict';

define([
	'routes/authenticated', 'models/group'
], function(AuthenticatedRoute, GroupModel) {
	var AdminGroupsIndexRoute = AuthenticatedRoute.extend({
		model: function() {
			return GroupModel.fetch();
		},
		setupController: function(controller, groups) {
			controller.set('groups', groups);
		}
	});
	return AdminGroupsIndexRoute;
});