'use strict';

define([
	'routes/authenticated', 'models/course', 'models/user'
], function(AuthenticatedRoute, CourseModel, UserModel) {
	var AdminCoursesNewRoute = AuthenticatedRoute.extend({
		model: function(params) {
			return Ember.RSVP.hash({
				allUsers: UserModel.fetch(),
				course: CourseModel.create({})
			});
		},
		setupController: function(controller, model) {
			this._super(controller, model.course);
			this.controllerFor('admin.course').setProperties({
				isNew: true,
				model: model.course,
				allUsers: model.allUsers
			});
		},
		renderTemplate: function() {
			this.render('admin.courses.form', {
				controller: 'admin.course'
			});
		}
	});
	return AdminCoursesNewRoute;
});
