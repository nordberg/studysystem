'use strict';

define([
	'routes/authenticated', 'models/course', 'models/user'
], function(AuthenticatedRoute, CourseModel, UserModel) {
	var AdminCoursesEditRoute = AuthenticatedRoute.extend({
		model: function(params) {
			return Ember.RSVP.hash({
				allUsers: UserModel.fetch(),
				course: CourseModel.fetch(params.course_id)
			});
		},
		setupController: function(controller, model) {
			this._super(controller, model.course);
			this.controllerFor('admin.course').setProperties({
				isNew: false,
				model: model.course,
				allUsers: model.allUsers
			});
		},
		renderTemplate: function(controller) {
			this.render('admin.courses.form', {
				controller: 'admin.course'
			});
		}
	});

	return AdminCoursesEditRoute;
});
