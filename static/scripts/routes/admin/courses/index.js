'use strict';

define([
	'routes/authenticated', 'models/course'
], function(AuthenticatedRoute, CourseModel) {
	var AdminCoursesIndexRoute = AuthenticatedRoute.extend({
		model: function() {
			return CourseModel.fetch();
		}
	});
	return AdminCoursesIndexRoute;
});