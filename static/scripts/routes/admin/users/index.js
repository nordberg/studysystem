'use strict';

define([
	'routes/authenticated', 'models/user'
], function(AuthenticatedRoute, UserModel) {
	var AdminUsersIndexRoute = AuthenticatedRoute.extend({
		model: function() {
			return UserModel.fetch();
		},
		setupController: function(controller, users) {
			controller.set('users', users);
		}
	});
	return AdminUsersIndexRoute;
});