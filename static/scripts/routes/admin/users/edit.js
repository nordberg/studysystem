'use strict';

define([
	'ember', 'routes/authenticated', 'models/user', 'models/group'
], function(Ember, AuthenticatedRoute, UserModel, GroupModel) {
	var AdminUsersEditRoute = AuthenticatedRoute.extend({
		model: function(params) {
			return Ember.RSVP.hash({
				allGroups: GroupModel.fetch(),
				user: UserModel.fetch(params.user_id)
			});
		},
		setupController: function(controller, model) {
			this.controllerFor('admin.user').setProperties({
				isNew: false,
				user: model.user,
				allGroups: model.allGroups
			});
		},
		renderTemplate: function() {
			this.render({
				controller: 'admin.user'
			});
		}
	});
	return AdminUsersEditRoute;
});
