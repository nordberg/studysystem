'use strict';

define([
	'ember', 'routes/authenticated', 'models/user', 'models/group'
], function(Ember, AuthenticatedRoute, UserModel, GroupModel) {
	var AdminUsersRegisterRoute = AuthenticatedRoute.extend({
		model: function(params) {
			return Ember.RSVP.hash({
				allGroups: GroupModel.fetch(),
				user: UserModel.create({})
			});
		},
		setupController: function(controller, model) {
			this.controllerFor('admin.user').setProperties({
				isNew: true,
				user: model.user,
				allGroups: model.allGroups
			});
		},
		renderTemplate: function() {
			this.render({
				controller: 'admin.user'
			});
		}
	});
	return AdminUsersRegisterRoute;
});
