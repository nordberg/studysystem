'use strict';

define([
	'routes/authenticated', 'models/document'
], function(AuthenticatedRoute, DocumentModel) {
	var AdminDocumentsEditRoute = AuthenticatedRoute.extend({
		model: function(params) {
			return DocumentModel.fetch(params.document_id);
		},
		setupController: function(controller, model) {
			this._super(controller, model);
			this.controllerFor('documents.edit').setProperties({
				model: model
			});
		},
		renderTemplate: function() {
			this.render('documents.edit');
		}
	});
	return AdminDocumentsEditRoute;
});