'use strict';

define(['routes/authenticated'], function(AuthenticatedRoute) {
	var AdminRoute = AuthenticatedRoute.extend({
		renderTemplate: function() {
			this.render('base');
			this.render('navs/admin', {
				into: 'base',
				outlet: 'nav'
			});
			this.render('admin', {
				into: 'base'
			});
		},
		activate: function() {
			if (!this.get('authManager.user.isAdmin')) {
				// this.transitionTo('courses');
			}
		}
	});
	return AdminRoute;
});