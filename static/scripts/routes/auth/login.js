'use strict';

define(['ember', 'utils/jsonRequest'], function(Ember, jsonRequest) {
	var LoginRoute = Ember.Route.extend({
		model: function() {
			return Ember.Object.create({
				email: '',
				password: ''
			});
		},
		activate: function() {
			var user = this.controllerFor('application').get('user');
			if (this.get('authManager').isAuthenticated()) {
				this.transitionTo('courses');
			}
		}
	});
	return LoginRoute;
});