'use strict';

define([
	'ember', 'routes/authenticated', 'utils/jsonRequest'
], function(Ember, AuthenticatedRoute, jsonRequest) {
	var LogoutRoute = AuthenticatedRoute.extend({
		activate: function() {
			if (!this.get('authManager').isAuthenticated()) {
				this.transitionTo('login');
			} else {
				var self = this;
				jsonRequest('/logout', {}).then(function(data) {
					self.get('authManager').reset();
					self.transitionTo('login');
				});
			}
			this._super();
		}
	});
	return LogoutRoute;
});