'use strict';

define(['ember'], function(Ember) {
	var ApplicationRoute = Ember.Route.extend({
		actions: {
			openModal: function(modalName, model) {
				Ember.$('body').addClass('without-scrolling');
				this.controllerFor(modalName).set('model', model);
				return this.render(modalName, {
					into: 'application',
					outlet: 'modal'
				});
			},

			closeModal: function() {
				Ember.$('body').removeClass('without-scrolling');
				return this.disconnectOutlet({
					outlet: 'modal',
					parentView: 'application'
				});
			}
		}
	});

	return ApplicationRoute;
});