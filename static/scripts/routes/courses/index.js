'use strict';

define(['routes/authenticated', 'models/course'], function(AuthenticatedRoute, CourseModel) {
	var CoursesRoute = AuthenticatedRoute.extend({
		model: function() {
			return this.modelFor('courses');
		},
		setupController: function(controller, courses) {
			this._super(controller, courses);
			console.log(courses)
			controller.set('courses', courses);
			this.controllerFor('courses').set('showBackButton', false);
		},
	});
	return CoursesRoute;
});
