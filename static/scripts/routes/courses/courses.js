'use strict';

define([
	'routes/authenticated', 'models/course'
], function(AuthenticatedRoute, CourseModel) {
	var CoursesRoute = AuthenticatedRoute.extend({
		model: function() {
			return CourseModel.fetch({
				published: true
			});
		},
		renderTemplate: function() {
			this.render('base');
			this.render('navs/common', {
				into: 'base',
				outlet: 'nav',
			});

		},
		actions: {
			willTransition: function() {
				this.controllerFor('courses').set('showBackButton', false);
			}
		}
	});
	return CoursesRoute;
});
