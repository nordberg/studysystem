'use strict';

define(['routes/authenticated'], function(AuthenticatedRoute) {
	var UsersRoute = AuthenticatedRoute.extend({
		renderTemplate: function() {
			this.render('base');
			this.render('navs/common', {
				into: 'base',
				outlet: 'nav',
			});
		},
		actions: {
			willTransition: function() {
				this.controllerFor('users').set('showBackButton', false);
			}
		}
	});
	return UsersRoute;
});
