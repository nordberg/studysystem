'use strict';

define([
	'routes/authenticated', 'models/user'
], function(
	AuthenticatedRoute, UserModel
) {
	var UserEditRoute = AuthenticatedRoute.extend({
		model: function(params) {
			return UserModel.fetch(params.user_id);	
		},
		setupController: function(controller, model) {
			this._super(controller, model);
			this.controllerFor('users.user').setProperties({
				model: model,
				showBackButton: true,
				goBackInfo: 'Назад'
			});
			this.controllerFor('users').set('showBackButton', true);
			this.controllerFor('users').set('goBackInfo', {
				text: 'Назад'
			})
		},
		renderTemplate: function(controller) {
			this.render('users.edit', {
				controller: 'users.user'
			});
		},
		actions: {
			goBack: function() {
				this.transitionTo('users.user');
			}
		}
	});
	return UserEditRoute;
});

