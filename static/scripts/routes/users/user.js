'use strict';

define([
	'routes/authenticated', 'models/user'
], function(
	AuthenticatedRoute, UserModel
) {
	var UserRoute = AuthenticatedRoute.extend({
		model: function(params) {
			return UserModel.fetch(params.user_id);	
		},
		setupController: function(controller, model) {
			controller.set('model', model);	
		}
	});
	return UserRoute;
});
