'use strict';

define([
	'routes/application',
	'routes/auth/login',
	'routes/auth/logout',

	'routes/course/course',
	'routes/course/index',
	'routes/course/materials',
	'routes/course/section/section',
	'routes/course/section/index',
	'routes/course/section/edit',
	'routes/course/subsection/subsection',
	'routes/course/subsection/index',
	'routes/course/subsection/edit',

	'routes/courses/index',
	'routes/courses/courses',

	'routes/users/users',
	'routes/users/user',
	'routes/users/edit',

	'routes/admin/admin',

	'routes/admin/courses/index',
	'routes/admin/courses/new',
	'routes/admin/courses/edit',

	'routes/admin/users/index',
	'routes/admin/users/edit',
	'routes/admin/users/register',

	'routes/admin/groups/index',
	'routes/admin/groups/new',
	'routes/admin/groups/edit',
	'routes/admin/documents/edit',
], function(
	Application,
	Login, Logout,

	Course, CourseIndex, CourseMaterials,
	Section,  SectionIndex, SectionEdit,
	Subsection, SubsectionIndex, SubsectionEdit,

	CoursesIndex, Courses,

	Users, User, UserEdit,
	Admin,
	AdminCoursesIndex, AdminCoursesNew, AdminCoursesEdit,
	AdminUsersIndex, AdminUsersEdit, AdminUsersRegister,
	AdminGroupsIndex, AdminGroupsNew, AdminGroupsEdit,
	AdminDocumentsEdit
) {
	//TODO make placeholder in deps and put all files from folders to id
	//after it iterate over all arguments and make dictionary
	return {
		ApplicationRoute: Application,
		LoginRoute: Login,
		LogoutRoute: Logout,

		CourseRoute: Course,
		CourseIndexRoute: CourseIndex,
		CourseMaterialsRoute: CourseMaterials,
		CourseSectionRoute: Section,
		CourseSectionIndexRoute: SectionIndex,
		CourseSectionEditRoute: SectionEdit,
		CourseSectionSubsectionRoute: Subsection,
		CourseSectionSubsectionIndexRoute: SubsectionIndex,
		CourseSectionSubsectionEditRoute: SubsectionEdit,

		CoursesIndexRoute: CoursesIndex,
		CoursesRoute: Courses,

		UsersRoute: Users,
		UsersUserRoute: User,
		UsersEditRoute: UserEdit,

		AdminRoute: Admin,

		AdminCoursesIndexRoute: AdminCoursesIndex,
		AdminCoursesNewRoute: AdminCoursesNew,
		AdminCoursesEditRoute: AdminCoursesEdit,
		AdminUsersIndexRoute: AdminUsersIndex,
		AdminUsersEditRoute: AdminUsersEdit,
		AdminUsersRegisterRoute: AdminUsersRegister,
		AdminGroupsIndexRoute: AdminGroupsIndex,
		AdminGroupsNewRoute: AdminGroupsNew,
		AdminGroupsEditRoute: AdminGroupsEdit,
		AdminDocumentsEditRoute: AdminDocumentsEdit
	};
});
