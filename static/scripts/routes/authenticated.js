'use strict';

define(['ember', 'utils/errors'], function(Ember, errors) {
	var AuthenticatedRoute = Ember.Route.extend({
		actions: {
			error: function(error, transition) {
				if (error.statusText == 'Unauthorized') {
					this.get('authManager').reset();
					this.transitionTo('login');
					return false;
				}
				return true;
			}
		}
	});
	return AuthenticatedRoute;
});