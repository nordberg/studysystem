'use strict';

define(['routes/authenticated'], function(AuthenticatedRoute) {
	var CourseindexRoute = AuthenticatedRoute.extend({
		model: function(params) {
			return this.modelFor('course');
		},
		setupController: function(controller, course) {
			this._super(controller, course);
			controller.set('course', course);
			this.controllerFor('courses').set('showBackButton', true);
			this.controllerFor('courses').set('goBackInfo', {
				text: 'К курсам'
			});
		}
	});
	return CourseindexRoute;
});