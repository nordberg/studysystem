'use strict';

define([
	'routes/authenticated', 'models/document'
], function(AuthenticatedRoute, DocumentModel) {
	var SubsectionIndex = AuthenticatedRoute.extend({
		model: function() {
			var model = this.modelFor('course.section.subsection');
			return DocumentModel.fetch(model.get('content.id')).then(function(doc) {
				return doc.loadContentForLastVersion().then(function(content) {
					return {
						markdown: content
					};
				});
			});
		},
		setupController: function(controller, content) {
			this._super(controller, content);
			controller.set('course', this.modelFor('course'));
			controller.set('section', this.modelFor('course.section'));
			controller.set('subsection', this.modelFor('course.section.subsection'));
			this.controllerFor('courses').set('showBackButton', true);
			this.controllerFor('courses').set('goBackInfo', {
				text: 'К курсу'
			});
		}
	});

	return SubsectionIndex;
});