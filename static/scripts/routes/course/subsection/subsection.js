'use strict';

define(['routes/authenticated'], function(AuthenticatedRoute) {
	var SubsectionRoute = AuthenticatedRoute.extend({
		model: function(params, transition) {
			var subsection = this.modelFor('course.section')
				.get('subsections')
				.findBy('alias', params.subsection_alias);
			return subsection;
		},
		actions: {
			goBack: function() {
				this.transitionTo(
					'course',
					this.modelFor('course').get('alias')
				);
			}
		}
	});

	return SubsectionRoute;
});