'use strict';

define([
	'routes/authenticated', 'models/document'
], function(AuthenticatedRoute, DocumentModel) {
	var SubsectionEditRoute = AuthenticatedRoute.extend({
		model: function() {
			var subsection = this.modelFor('course.section.subsection');
			return DocumentModel.fetch(subsection.get('content.id'));
		}
	});

	return SubsectionEditRoute;
});