'use strict';

define(['routes/authenticated'], function(AuthenticatedRoute) {
	return AuthenticatedRoute.extend({
		model: function() {
			return this.modelFor('course').get('materials');
		}
	});
});