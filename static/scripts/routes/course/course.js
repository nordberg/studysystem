'use strict';

define([
	'routes/authenticated', 'models/course'
], function(AuthenticatedRoute, CourseModel) {
	var CourseRoute = AuthenticatedRoute.extend({
		model: function(params) {
			return CourseModel.fetch({
				alias: params.course_alias
			}).then(function(courses) {
				return courses.get('firstObject');
			});
		},
		actions: {
			goBack: function() {
				this.transitionTo('courses');
				return false;
			}
		}
	});
	return CourseRoute;
});