'use strict';

define(['routes/authenticated'], function(AuthenticatedRoute) {
	var SectionIndex = AuthenticatedRoute.extend({
		model: function() {
			return this.modelFor('course.section');
		},
		setupController: function(controller, section) {
			this._super(controller, section);
			controller.set('course', this.modelFor('course'));
			controller.set('section', this.modelFor('course.section'));
			controller.set('subsection', this.modelFor('course.section.subsection'));
			this.controllerFor('courses').set('showBackButton', true);
			this.controllerFor('courses').set('goBackInfo', {
				text: 'К курсу'
			});
		}
	});

	return SectionIndex;
});
