'use strict';

define(['routes/authenticated'], function(AuthenticatedRoute) {
	var SectionRoute = AuthenticatedRoute.extend({
		model: function(params, transition) {
			var section = this.modelFor('course')
				.get('sections')
				.findBy('alias', params.section_alias);
			return section;
		},
		actions: {
			goBack: function() {
				this.transitionTo(
					'course',
					this.modelFor('course').get('alias')
				);
			}
		}
	});

	return SectionRoute;
});
