'use strict';

define(['routes/authenticated'], function(AuthenticatedRoute) {
	var SectionEdit = AuthenticatedRoute.extend({
		model: function() {
			return this.modelFor('course.section');
		},
		setupController: function(controller, section) {
			this._super(controller, section);
			this.controllerFor('course.section.index').setProperties({
				course: this.modelFor('course'),
				// section: this.modelFor('course.section'),
				section: section
			});
			this.controllerFor('courses').set('showBackButton', true);
			this.controllerFor('courses').set('goBackInfo', {
				text: 'К курсу'
			});
		},
		renderTemplate: function() {
			this.render({
				controller: 'course.section.index'
			});
		}
	});

	return SectionEdit;
});
