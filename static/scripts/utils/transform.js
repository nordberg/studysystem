'use strict';

define(['ember', 'marked'], function(Ember, marked) {
	var Transformer = function() {};

	Transformer.prototype.mdToHtml = function(input) {
		return marked(input, {
			gfm: true,
			tables: true,
			sanitize: true
		});
	};

	return new Transformer();
});