'use strict';

define([
	'ember', 'handlebars', 'moment', 'utils/transform'
], function(Ember, Handlebrs, moment, Transform) {
	Ember.Handlebars.registerBoundHelper('formatDate', function(date, format) {
		return moment(date).format('YYYY-MM-DD HH:mm');
	});
	Ember.Handlebars.registerBoundHelper('markdown', function(html) {
		return new Handlebars.SafeString(Transform.mdToHtml(html));
	});
});