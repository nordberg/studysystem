'use strict';

define(['jquery'], function($) {
	var jsonRequest = function(url, params) {
		return $.ajax({
			type: 'post',
			url: url,
			data: JSON.stringify(params),
			contentType: 'application/json',
			dataType: 'json'
		});
	};
	return jsonRequest;
});