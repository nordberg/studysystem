'use strict';

define(['ember'], function(Ember) {
	var AlertWrap = function() {};

	AlertWrap.prototype.show = function(type, message) {
		var alertView = Ember.View.views['main-alert'];
		if (alertView) {
			alertView.show(type, message)
		} else {
			alert(message);
		}
	};

	AlertWrap.prototype.hide = function() {
		Ember.View.views['main-alert'].hide();
	};

	AlertWrap.prototype.showConfirm = function(message, approveCallback, rejectCallback) {
		var confirmDialogView = Ember.View.views['confirm-dialog'];
		if (confirmDialogView) {
			Ember.View.views['confirm-dialog'].show(
				message, approveCallback, rejectCallback
			);
		} else {
			if (confirm(message)) {
				approveCallback();
			} else {
				rejectCallback();
			}
		}
	};

	return new AlertWrap();
});