'use strict';

define(['ember'], function(Ember) {
	var Error = Ember.Object.extend({});
	var AutheticationError = Error.extend({
		name: 'AutheticationError'
	});
	return {
		AutheticationError: AutheticationError
	};
});