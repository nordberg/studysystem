'use strict';

define([
	'models/user',
	'models/group',
	'models/course',
	'models/document'
], function(
	User, Group, Course, Document
) {
	//TODO make placeholder in deps and put all files from folders to id
	//after it iterate over all arguments and make dictionary
	return {
		User: User,
		Group: Group,
		Course: Course,
		Document: Document
	};
});