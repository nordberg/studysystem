'use strict';

define(['ember', 'models/group'], function(Ember, Group) {

	var UserModel = Ember.Model.extend({
		id: Ember.attr(),
		firstName: Ember.attr(String),
		lastName: Ember.attr(String),
		email: Ember.attr(String),
		password: Ember.attr(String),
		repassword: Ember.attr(String),
		groups: Ember.hasMany(Group, { embedded: false, key: 'groups_ids'}),
		image: Ember.attr(),

		fullName: function() {
			return this.get('firstName') + ' ' + this.get('lastName');
		}.property('firstName', 'lastName'),

		isAdmin: function() {
			return this.get('groups').some(function(group) {
				return group.get('name') == 'admin';
			});
		}.property('groups.@each.name'),
		isProfessor: function() {
			return this.get('groups').some(function(group) {
				return group.get('name') == 'professor';
			});
		}.property('groups.@each.name')
	});

	UserModel.url = '/api/users';
	UserModel.rootKey = 'user';
	UserModel.collectionKey = 'users';

	return UserModel;
});
