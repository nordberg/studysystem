'use strict';

define([
	'ember', 'models/user', 'models/document', 'utils/jsonRequest'
], function(Ember, UserModel, DocumentModel, jsonRequest) {

	var SubsectionTypes = [{
			type: 'lection',
			title: 'Лекция'
		}, {
			type: 'lab',
			title: 'Лабораторная работа'
		}, {
			type: 'practice',
			title: 'Практическое занятие'
		}
	];
	var CourseSectionSubsectionContent = Ember.Model.extend({
		id: Ember.attr(Number),
		type: Ember.attr(String),

		isDocument: function() {
			return this.get('type') == 'document';
		}.property('type'),

		fetchValue: function() {
			return DocumentModel.fetch(this.get('id')).then(function(doc) {
				return doc.loadContentForLastVersion();
			});
		}
	});

	var CourseSectionSubsection = Ember.Model.extend({
		title: Ember.attr(String),
		type: Ember.attr(String),
		order: Ember.attr(Number),
		alias: Ember.attr(String),
		published: Ember.attr(Boolean),

		typeName: function() {
			var type = SubsectionTypes.findBy('type', this.get('type'));
			return type ? type.title : '';
		}.property('type'),

		types: function() {
			return SubsectionTypes;
		}.property(),

		content: Ember.belongsTo(DocumentModel, {
			embedded: false, key: 'content'
		}),
	});

	CourseSectionSubsection.url = '/api/subsections';
	CourseSectionSubsection.rootKey = 'subsection';
	CourseSectionSubsection.collectionKey = 'subsections';

	var CourseSection = Ember.Model.extend({
		title: Ember.attr(String),
		description: Ember.attr(String),
		duration: Ember.attr(Number),
		alias: Ember.attr(String),
		published: Ember.attr(Boolean),
		subsections: Ember.hasMany(CourseSectionSubsection, { embedded: false, key: 'subsections_ids'})
	});

	CourseSection.url = '/api/sections';
	CourseSection.rootKey = 'section';
	CourseSection.collectionKey = 'sections';

	var CourseMaterial = Ember.Model.extend({
		title: Ember.attr(String),
		description: Ember.attr(String),
		link: Ember.attr(String)
	});

	var CourseModel = Ember.Model.extend({
		title: Ember.attr(String),
		alias: Ember.attr(String),
		description: Ember.attr(String),
		published: Ember.attr(Boolean),
		authors: Ember.hasMany(UserModel, {embedded: false, key: 'authors_ids' }),
		sections: Ember.hasMany(CourseSection, { embedded: false, key: 'sections_ids' }),
		materials: Ember.hasMany(CourseMaterial, { embedded: true, key: 'materials' })
	});

	CourseModel.url = '/api/courses';
	CourseModel.rootKey = 'course';
	CourseModel.collectionKey = 'courses';

	return CourseModel;
});
