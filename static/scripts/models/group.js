'use strict';

define(['ember'], function(Ember) {
	var GroupModel = Ember.Model.extend({
		id: Ember.attr(),
		name: Ember.attr(String),
		description: Ember.attr(String),
		title: Ember.attr(String)
	});

	GroupModel.url = '/api/groups';
	GroupModel.rootKey = 'group';
	GroupModel.collectionKey = 'groups';

	return GroupModel;
});
