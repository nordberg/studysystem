'use strict';

define([
	'ember', 'models/user', 'utils/jsonRequest'
], function(Ember, UserModel, jsonRequest) {
	var StateInfo = Ember.Model.extend({
		user: Ember.belongsTo(UserModel, { key: 'user' }),
		date: Ember.attr(Date)
	});

	var DocumentVersion = Ember.Model.extend({
		author: Ember.belongsTo(UserModel, { key: 'author' }),
		name: Ember.attr(String),
		created: Ember.attr(Date),
		state: Ember.attr(String),
		stateInfo: Ember.belongsTo(StateInfo, {
			embedded: true, key: 'stateInfo'
		}),

		confirmed: function() {
			return this.get('state') === 'confirmed';
		}.property('state'),

		notDefined: function() {
			return this.get('state') === 'undefined';
		}.property('state'),

		rejected: function() {
			return this.get('state') === 'rejected';
		}.property('state')
	});

	var DocumentModel = Ember.Model.extend({
		id: Ember.attr(Number),
		created: Ember.attr(Date),
		title: Ember.attr(String),
		authors: Ember.hasMany(UserModel, { key: 'authors_ids' }),
		versions: Ember.hasMany(DocumentVersion, {embedded: true, key: 'versions'}),
		description: Ember.attr(String),

		content: null,

		versionsList: function() {
			return this.get('versions').toArray().reverse().slice(0, 10);
		}.property('versions.@each').cacheable(),

		//This placed here because we don't have document id in version and can't
		//load it's content. Maybe we have to think here
		_buildVersionUrl: function(versionName) {
			return [
				DocumentModel.url,
				this.get('id'),
				'versions',
				versionName
			].join('/');
		},

		loadContentForVersion: function(version) {
			Ember.assert('Name must be specified', version);

			var url = this._buildVersionUrl(version.get('name')),
				self = this;

			return Ember.$.ajax(url, { type: 'get' }).then(function(result) {
				Ember.run(self, 'set', 'content', result.version.raw);
				return result.version.raw;
			});
		},

		lastVersion: function() {
			return this.get('versions').get('lastObject');
		}.property('versions.@each'),

		loadContentForLastVersion: function() {
			var version = this.get('lastVersion');
			return this.loadContentForVersion(version);
		},

		createVersion: function(content) {
			var url = this._buildVersionUrl(),
				self = this;
			return Ember.$.ajax(url, {
				type: 'post',
				data: { content: content }
			}).then(function(result) {
				var data = result.version;
				var version = self.get('versions').create({});

				version.load(data.name, data);

				return version;
			});
		},

		confirmVersion: function(version) {
			var url = this._buildVersionUrl(version.get('name'));
			jsonRequest(url, { action: 'confirm' }).then(function(result) {
				var data = result.version;
				version.load(data.name, data)
				return version;
			});
		},

		rejectVersion: function(version) {
			var url = this._buildVersionUrl(version.get('name'));
			jsonRequest(url, { action: 'reject' }).then(function(result) {
				var data = result.version;
				version.load(data.name, data)
				return version;
			});
		},

		unconfirmedVersions: function() {
			return this.get('versions').filterBy('state', 'rejected');
		}.property('versions.@each.state'),

		toJSON: function() {
			var rootKey = Ember.get(this.constructor, 'rootKey');
			var response = {};

			response[rootKey] = {
				title: this.get('title'),
				description: this.get('description'),
				authors_ids: this.get('authors').toJSON()
			};

			return response;
		}
	});

	DocumentModel.url = '/api/documents';
	DocumentModel.rootKey = 'document';
	DocumentModel.collectionKey = 'documents';

	return DocumentModel;
});
