'use strict';

define(['ember'], function(Ember) {
	Ember.Test.registerAsyncHelper('assertExists', function(app, selector, times) {
		if (Ember.isNone(times)) {
			times = 1;
		}
		return wait()
			.find(selector)
			.then(function(element) {
				equal(
					element.length,
					times,
					['found', selector, element.length, 'times', 'but expected', times].join(' ')
				);
			});
	});

	Ember.Test.registerAsyncHelper('login', function(app, role) {
		role = role || 'admin';
		var email;
		switch (email) {
			case 'student': email = 's@example.com';
			break;
			case 'professor': email = 'p@example.com';
			break;
			default: email = 'a@example.com';
		};

		visit('/login').then(function() {
			fillIn('.login__form__input[name="email"]', 'a@example.com');
			fillIn('.login__form__input[name="password"]', '1234');
			return click('.login__form__submit');
		})
		return wait();
	});
});

