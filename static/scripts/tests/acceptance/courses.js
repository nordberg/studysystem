'use strict';

define(['jquery'], function($) {
	module('Integration test -- Courses', {
		setup: function() {
			Ember.run(StudySystem, StudySystem.advanceReadiness);
		},
		tearDown: function() {
			StudySystem.reset();
		}
	});

	test('courses page existing all blocks test', function() {
		visit('/courses');
		andThen(function() {
			assertExists('.small-profile__fullname');
			assertExists('.small-profile__image');
		});
	});

	test('courses count test', function(){
		visit('/courses').then(function() {
			equal(
				find('.course-card').length,
				3,
				'Courses count have to be 3'
			);
		});
	});

	var checkAuthors = function(authorsLinks) {
		equal(authorsLinks.length, 2, 'Have to be 2 authors');
		equal(authorsLinks[0].text, 'Василий Петров', 'Invalid author at 0 position');
		equal(authorsLinks[1].text, 'Иван Иванов', 'Invalid author at 1 position');
	};

	test('authors check', function() {
		visit('/courses');
		andThen(function() {
			var siaod = $(find('.course-card')[0]);
			var authorsLinks = siaod.find('.course-card__authors__item a');
			checkAuthors(authorsLinks);
		});
	});

	test('course page test', function() {
		visit('/courses');
		andThen(function() {
			click('.course-card__header:first a');
			andThen(function() {
				checkAuthors(find('.course-card__authors__item a'));
				var sections = find('.course-section');
				equal(sections.length, 2, 'Sections count should be 2');
				sections.each(function(inx, section) {
					equal($(section).find('.subsection-link').length, 1);
				});
			});
		});
	});
});