'use strict';

define([], function() {
	module('Integration Tests - Login', {
		setup: function() {
			Ember.run(StudySystem, StudySystem.advanceReadiness);
		},
		tearDown: function() {
			StudySystem.reset();
		}
	});

	//TODO This is race between login and courses i don't khow why

	// test('test failure login', function(){
	// 	visit('/login').then(function() {
	// 		ok(exist('.login__form__input[name="email"]'), 'Email field doesnt exist');
	// 		ok(exist('.login__form__input[name="password"]'), 'Password field doesn\'t exist');
	// 		fillIn('.login__form__input[name="email"]', 'a@example.com')
	// 		fillIn('.login__form__input[name="password"]', '5234')
	// 		return click('.login__form__submit').then(function() {
	// 			ok(exist('.login__form__error span'), 'Message not have to exist');
	// 		});
	// 	});
	// });

	test('test success login', function(){
		visit('/login').then(function() {
			assertExists('.login__form__input[name="email"]');
			assertExists('.login__form__input[name="password"]');
			fillIn('.login__form__input[name="email"]', 'a@example.com')
			fillIn('.login__form__input[name="password"]', '1234')
			return click('.login__form__submit').then(function() {
				assertExists('.login__form__error span', 0);
			});
		});
	});
});