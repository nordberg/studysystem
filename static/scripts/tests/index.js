'use strict';

define([], function() {
	require(['tests/helpers']);
	//All test will start just after login
	require(['tests/acceptance/login'], function() {
		require(['tests/acceptance/courses']);
	});
});