'use strict';

define([
	'serializers/user', 'serializers/group'
], function(
	UserSerializer, GroupSerializer
) {
	return {
		UserSerializer: UserSerializer,
		GroupSerializer: GroupSerializer
	};
});