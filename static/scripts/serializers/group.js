'use strict';

define(['ember-data'], function(DS) {
	var GroupSerializer = DS.RESTSerializer.extend({
		extractArray: function(store, type, payload, id, requestType) {
			payload.groups.forEach(function(group) {
				group.users = group.users.mapBy('id');
			});
			return this._super(store, type, payload, id, requestType);
		},

		extractSingle: function(store, type, payload, id, requestType) {
			payload.group.users = payload.group.users.mapBy('id');
			return this._super(store, type, payload, id, requestType);
		}
	});
	return GroupSerializer;
});