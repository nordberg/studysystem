'use strict';

define(['ember-data'], function(DS) {
	var UserSerializer = DS.RESTSerializer.extend({
		// TODO Recursively check is payload field array or object and restructure it
		extractArray: function(store, type, payload, id, requestType) {
			payload.users.forEach(function(user) {
				user.groups = user.groups.mapBy('id');
			});

			return this._super(store, type, payload, id, requestType);
		},

		extractSingle: function(store, type, payload, id, requestType) {
			payload.user.groups = payload.user.groups.mapBy('id');
			return this._super(store, type, payload, id, requestType);
		}
	});
	return UserSerializer;
});