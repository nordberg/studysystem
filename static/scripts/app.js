'use strict';

define(['ember', 'index', 'utils/handlebarsHelpers'], function(Ember, application) {
	var core = Ember.merge(
		application,
		{
			LOG_TRANSITIONS: true,
			LOG_VIEW_LOOKUPS: true,
			ready: function() {
				this.register(
					'authManager:current',
					this.AuthManager,
					{ singleton: true }
				);
				this.inject(
					'route',
					'authManager',
					'authManager:current'
				);
				this.inject(
					'controller',
					'authManager',
					'authManager:current'
				);
			}
		}
	);

	var App = Ember.Application.extend(core);

	return App;
});