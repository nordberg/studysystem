'use strict';

requirejs.config({
	baseUrl: '/static/scripts',
	shim: {
		'ember': {
			deps: ['jquery', 'handlebars', 'emblem'],
			exports: 'Ember'
		},
		'ember-model': ['ember'],
		'emblem': {
			exports: 'Emblem'
		},
		'templates': {
			deps: ['ember']
		},
		'handlebars': {
			exports: 'Handlebars'
		}
	},
	paths: {
		text: 'libs/text',
		ember: 'libs/ember',
		'ember-model': 'libs/ember-model',
		jquery: 'libs/jquery-1.10.2.min',
		handlebars: 'libs/handlebars-1.0.0',
		emblem: 'libs/emblem',
		moment: 'libs/moment',
		ace: 'libs/ace',
		marked: 'libs/marked'
	}
});

require([
	'ember', 'ember-model', 'adapter', 'tests/helpers', 'templates'
], function(Ember, EmberModel, Adapter) {
	//Here we will config ember global options
	Ember.Model.reopenClass({
		adapter: Adapter.create({})
	});

	require(['app', 'router'], function(App, Router) {
		Ember.testing = true;

		Ember.run(function(){
			Ember.run(localStorage, 'clear');

			App = App.create({
				rootElement: '#ember-testing'
			});

			App.Router = Router;

			App.Router.reopen({
				location: 'none'
			});

			App.setupForTesting();
			App.injectTestHelpers();
		});

		App.reset();

		window['StudySystem'] = App;

		require(['tests/index']);
	});
});