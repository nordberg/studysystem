'use strict';

define([
	'components/modal',
	'components/tabs/view',
	'components/tabs/item',
	'components/tabs/pane'
], function(
	ModalDialogComponent,
	TabsViewComponent,
	TabsViewItemComponent,
	TabsViewPaneComponent
) {
	return {
		ModalDialogComponent: ModalDialogComponent,
		TabsViewComponent: TabsViewComponent,
		TabsViewItemComponent: TabsViewItemComponent,
		TabsViewPaneComponent: TabsViewPaneComponent
	};
});
