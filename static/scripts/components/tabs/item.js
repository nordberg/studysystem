'use strict';

define(['ember'], function(Ember) {
	var TabsViewItem = Ember.Component.extend({
		classNames: ['tabs-view__item'],
		classNameBindings: ['isActive:tabs-view__item_active'],

		didInsertElement: function() {
			this.get('parentView.panes').pushObject({
				id: this.get('elementId'),
				title: this.get('title')
			});
		},

		isActive: function() {
			return this.get('parentView.activeTab') == this.get('elementId');
		}.property('elementId', 'parentView.activeTab')
	});

	return TabsViewItem;
});