'use strict';

define(['ember'], function(Ember) {
	var TabsViewPane = Ember.Component.extend({
		tagName: 'li',

		classNames: ['tabs-view__pane__item'],
		classNameBindings: ['isActive:tabs-view__pane__item_active'],

		isActive: function() {
			return this.get('parentView.activeTab') == this.get('tabId');
		}.property('tabId', 'parentView.activeTab'),

		click: function() {
			this.get('parentView').set('activeTab', this.get('tabId'));
		}
	});

	return TabsViewPane;
});