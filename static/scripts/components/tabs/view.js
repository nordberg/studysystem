'use strict';

define(['ember'], function(Ember) {
	var TabsView = Ember.Component.extend({
		classNames: ['tabs-view'],

		didInsertElement: function() {
			this.set('panes', []);
		},

		initPanes: function() {
			if (!this.get('activeTab')) {
				this.set('activeTab', this.get('panes.firstObject.id'));
			}
		}.observes('panes.[]')
	});
	return TabsView;
});