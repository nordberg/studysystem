'use strict';

define(['ember'], function(Ember) {
	var ModalDialogComponent = Ember.Component.extend({
		actions: {
			close: function() {
				return this.sendAction();
			}
		}
	});
	return ModalDialogComponent;
});