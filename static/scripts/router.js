'use strict';

define(['ember'], function(Ember) {
	var Router = Ember.Router.extend();

	Router.map(function() {
		this.route('login');
		this.route('logout');

		this.resource('admin', function() {
			this.resource('admin.courses', {path: '/courses'}, function() {
				this.route('new');
				this.resource(
					'admin.courses.edit',
					{ path: '/:course_id/edit' },
					function() {
						this.route('admin.courses.edit.new.document', '/documents/new')
					}
				);
			});
			this.resource('admin.users', {path: '/users'}, function() {
				this.route('edit', { path: '/:user_id/edit' });
				this.route('register');
			});
			this.resource('admin.groups', {path: '/groups'}, function() {
				this.route('new');
				this.route('edit', { path: '/:group_id/edit' });
			});
			this.resource('admin.documents', {path: '/documents'}, function() {
				this.route('new');
				this.route('edit', { path: '/:document_id/edit' });
			});
		});

		this.resource('courses', function() {
			this.resource('course', { path: '/:course_alias' }, function() {
				this.resource('course.section', { path: '/:section_alias'}, function() {
					this.resource('course.section.subsection', {path: '/:subsection_alias'}, function() {
						this.route('edit');
					});
					this.route('edit');
				});
				this.route('materials');
			});
		});

		this.resource('users', function() {
			this.route('user', { path: '/:user_id' });
			this.route('edit', { path: '/:user_id/edit' });
		});
	});


	return Router;
});
