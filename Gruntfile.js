'use strict';
var path = require('path');

module.exports = function(grunt) {
	grunt.initConfig({
		emblem: {
			compile: {
				files: {
					'static/scripts/templates.js': 'static/scripts/templates/**/*.emblem'
				},
				options: {
					root: 'static/scripts/templates/',
					dependencies: {
						jquery: 'static/scripts/libs/jquery-1.10.2.min.js',
						ember: 'static/scripts/libs/ember.js',
						emblem: 'static/scripts/libs/emblem.js',
						handlebars: 'static/scripts/libs/handlebars-1.0.0.js'
					}
				}
			}
		},
		less: {
			development: {
				options: {
					paths: ['static/less']
				},
				files: {
					'static/css/result.css': 'static/less/screen.less'
				}
			}
		},
		watch: {
			templates: {
				options: {
					livereload: true,
					spawn: false
				},
				files: ['static/scripts/templates/**/*.emblem'],
				tasks: ['emblem']
			},
			less: {
				options: {
					livereload: true,
					spawn: false
				},
				files: ['static/less/**/*.less'],
				tasks: ['less']
			}
		},
		concurrent: {
			dev: {
				tasks: ['nodemon', 'watch'],
				options: {
					logConcurrentOutput: true
				}
			}
		},
		nodemon: {
			dev: {
				options: {
					file: 'app.js'
				}
			}
		},
		shell: {
			runTests: {
				options: {
					stdout: true,
					stderr: true
				},
				command: 'NODE_ENV=test mocha'
			},
			makeDevData: {
				options: {
					stdout: true,
					stderr: true
				},
				command: 'node ./migrations/test_init.js'
			}
		}
	});

	grunt.loadNpmTasks('grunt-shell');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-emblem');
	grunt.loadNpmTasks('grunt-concurrent');
	grunt.loadNpmTasks('grunt-nodemon');

	grunt.registerTask('serve', ['emblem', 'less', 'concurrent:dev']);
	grunt.registerTask('test', ['shell:runTests']);
	grunt.registerTask('makeDevData', ['shell:makeDevData']);
};