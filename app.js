'use strict';

var express = require('express'),
	conform = require('conform'),
	config = require('./config'),
	app = express();

app.use('/static', express.static(__dirname + '/static'));

//Conform global options installing
conform.validate.defaults.additionalProperties = false;
conform.validate.defaults.cast = true;
conform.validate.defaults.castSource = true;
conform.validate.defaults.applyDefaultValue = true;

app.use(express.logger('dev'));
app.use(express.methodOverride());
app.use(express.json());
app.use(express.urlencoded());
app.use(express.cookieParser());

app.engine('jade', require('jade').__express);
app.set('view engine', 'jade');

app.use(require('./middleware/resExtend'));
app.use(require('./middleware/reqExtend'));
app.use(require('./middleware/session')());
app.use(require('./middleware/auth'));

require('./resources').register(app);

app.get('*', function(req, res) {
	res.render('index', {
		env: config.env
	});
});

app.listen(3000);