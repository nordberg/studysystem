'use strict';

var BaseModel = require('./base'),
	_ = require('underscore'),
	fs = require('fs'),
	path = require('path'),
	uuid = require('node-uuid'),
	config = require('../config'),
	Steppy = require('twostep').Steppy,
	extend = require('../utils/extend');

var FileModel = function() {
};

FileModel.prototype.insert = function(data, callback) {
	var fileName = uuid.v4(),
		filePath = path.join(config.documents.path, fileName);

	fs.writeFile(filePath, data, function(err) {
		if (err) {
			callback(err);
		} else {
			callback(null, fileName);
		}
	});
};

FileModel.prototype.update = function(fileName, data, callback) {
	throw new Error('method not implemented yet');
};

FileModel.prototype.findOne = function(params, callback) {
	fs.readFile(
		path.join(config.documents.path, params.name),
		{ encoding: 'utf8' },
		function(err, content) {
			if (err) {
				callback(err);
			} else {
				callback(null, {
					raw: content
				});
			}
		}
	);
};

FileModel.prototype.find = function(fileName, data, callback) {
	throw new Error('method not implemented yet');
};

module.exports = new FileModel();

