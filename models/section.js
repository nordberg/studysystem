'use strict';

var BaseModel = require('./base'),
	_ = require('underscore'),
	extend = require('../utils/extend');

var SectionModel = function() {
	SectionModel.super.constructor.call(this, 'sections');
};

extend(SectionModel, BaseModel);

module.exports = new SectionModel();

