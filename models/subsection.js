'use strict';

var BaseModel = require('./base'),
	_ = require('underscore'),
	extend = require('../utils/extend');

var SubsectionModel = function() {
	SubsectionModel.super.constructor.call(this, 'subsections');
};

extend(SubsectionModel, BaseModel);

module.exports = new SubsectionModel();

