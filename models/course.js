'use strict';

var BaseModel = require('./base'),
	Steppy = require('twostep').Steppy,
	extend = require('../utils/extend');

var CourseModel = function() {
	CourseModel.super.constructor.call(this, 'courses');
};

extend(CourseModel, BaseModel);

// How we need to remake this?
var filterByPublish = function(record, isPublished) {
	if (typeof isPublished !== 'undefined') {
		// record.sections = record.sections.filter(function(section) {
		// 	if (section.published == isPublished) {
		// 		section.subsections = section.subsections.filter(function(ss) {
		// 			return ss.published == isPublished;
		// 		});
		// 		return true;
		// 	}
		// 	return false;
		// });
	}

	return record;
};

CourseModel.prototype.findOne = function(params, projection, callback) {
	CourseModel.super.findOne.call(
		this,
		params,
		projection,
		function(err, record) {
			if (err) return callback(err);

			callback(err, filterByPublish(record, params.published));
		}
	);
};

CourseModel.prototype.find = function(params, projection, callback) {
	CourseModel.super.find.call(
		this,
		params,
		projection,
		function(err, records) {
			if (err) return callback(err);

			var carryPublish = function(record) {
				return filterByPublish(record, params.published);
			};

			callback(err, records.map(carryPublish));
		}
	);
};

module.exports = new CourseModel();

