'use strict';

var MongoClient = require('mongodb').MongoClient,
	assert = require('assert'),
	Steppy = require('twostep').Steppy,
	_ = require('underscore'),
	config = require('../config');

var	sequencesCollection = 'sequences';

var BaseModel = function(collectionName) {
	this.collectionName = collectionName;
	this.defaultProjection = {
		_id: 0,
		deleted: 0
	};
	this.defaultFindParams = {
		deleted: false
	};
};

BaseModel.prototype.prepareRecord = function(record) {
	if (record) {
		delete record._id;
	}
	return record;
};

BaseModel.prototype.getDb = function(callback) {
	if (this.db) {
		callback(null, this.db);
	} else {
		var self = this;
		Steppy(
			function() {
				MongoClient.connect(config.database.path, this.slot());
			},
			function(err, db) {
				this.pass(db);
				var indexesGroup = this.makeGroup();
				db.ensureIndex(
					'users',
					{ email: 1 },
					{ unique: true },
					indexesGroup.slot()
				);
			},
			function(err, db, indexes) {
				self.db = db;
				this.pass(db);
			},
			callback
		);
	}
};

BaseModel.prototype.prepereFindQuery = function(params) {
	return _.chain(this.defaultFindParams).clone().extend(params).value();
};

BaseModel.prototype.find = function(params, projection, callback) {
	console.assert(this.collectionName, 'Collection name must be specified');

	if (typeof projection === 'function') {
		callback = projection;
		projection = this.defaultProjection;
	} else {
		projection = _(_(this.defaultProjection).clone()).extend(projection);
	}

	var collectionName = this.collectionName,
		self = this;
	this.getDb(function(err, db) {
		if (err) return callback(err);

		var collection = db.collection(collectionName);
		collection.find(self.prepereFindQuery(params), projection)
			.sort('id')
			.toArray(function(err, results) {
				if (collectionName == 'sections') {
					console.log(results)
				}
				callback(err, results);
			});
	});
};

BaseModel.prototype.findOne = function(params, projection, callback) {
	console.assert(this.collectionName, 'Collection name must be specified');

	if (typeof projection === 'function') {
		callback = projection;
		projection = this.defaultProjection;
	} else {
		projection = _(_(this.defaultProjection).clone()).extend(projection);
	}

	var collectionName = this.collectionName,
		baseModel = this;
	this.getDb(function(err, db) {
		if (err) return callback(err);

		var collection = db.collection(collectionName),
			self = this;
		collection.findOne(
			baseModel.prepereFindQuery(params),
			projection,
			function(err, item) {
				callback(err, baseModel.prepareRecord(item));
			}
		);
	});
};

BaseModel.prototype.insert = function(data, callback) {
	console.assert(this.collectionName, 'Collection name must be specified');
	var collectionName = this.collectionName,
		baseModel = this;
	this.getDb(function(err, db) {
		if (err) return callback(err);

		var collection = db.collection(collectionName),
			self = this;
		baseModel.getId(function(err, id) {
			if (err) {
					callback(err);
			} else {
				data.id = id;
				data.deleted = false;
				collection.save(data, function(err, item) {
					callback(err, baseModel.prepareRecord(item));
				});
			}
		});
	});
};

BaseModel.prototype.count = function(params, callback) {
	var collectionName = this.collectionName,
		self = this;
	Steppy(
		function() {
			self.getDb(this.slot());
		},
		function(err, db) {
			db.collection(collectionName).count(params, this.slot());
		},
		callback
	);
};

BaseModel.prototype.beforeUpdate = function(criteria, data, callback) {
	callback(null, data);
};

BaseModel.prototype.buildUpdateQuery = function(data) {
	if (data.$set || data.$push) {
		return data;
	} else {
		return { $set : data };
	}
};

BaseModel.prototype.update = function(criteria, data, projection, callback) {
	console.assert(this.collectionName, 'Collection name must be specified');

	if (typeof projection === 'function') {
		callback = projection;
		projection = this.defaultProjection;
	} else {
		projection = _(_(this.defaultProjection).clone()).extend(projection);
	}

	var self = this;
	Steppy(
		function() {
			self.getDb(this.slot());
		},
		function(err, db) {
			this.pass(db, criteria);
			self.beforeUpdate(criteria, data, this.slot());
		},
		function(err, db, criteria, data) {
			db.collection(self.collectionName).findAndModify(
				criteria,
				[],
				self.buildUpdateQuery(data),
				{ new: true, fields: projection },
				this.slot()
			);
		},
		function(err, result) {
			self.afterUpdate(result, this.slot());
		},
		function(err, result) {
			this.pass(result);
			if (self.notify) {
				self.notify(result, self.UpdateAspect, this.slot());
			}
		},
		function(err, result) {
			this.pass(result);
		},
		callback
	);
};
BaseModel.prototype.updateAll = function(criteria, data, callback) {
	console.assert(this.collectionName, 'Collection name must be specified');
	var self = this;
	Steppy(
		function() {
			self.getDb(this.slot());
		},
		function(err, db) {
			this.pass(db, criteria);
			self.beforeUpdate(criteria, data, this.slot());
		},
		function(err, db, criteria, data) {
			db.collection(self.collectionName).update(
				criteria, { $set: data }, { multi: true }, this.slot()
			);
		},
		function(err, result) {
			self.afterUpdate(result, this.slot());
		},
		function(err, result) {
			this.pass(result);
			self.notify(result, self.UpdateAspect, this.slot());
		},
		function(err, result) {
			this.pass(result);
		},
		callback
	);
};

BaseModel.prototype.afterUpdate = function(result, callback) {
	callback(null, this.prepareRecord(result));
};

BaseModel.prototype.remove = function(criteria, callback) {
	console.assert(this.collectionName, 'Collection name must be specified');
	console.assert(this.notify, 'Model should have notify method');

	var collectionName = this.collectionName,
		self = this;

	Steppy(
		function() {
			self.getDb(this.slot());
		},
		function(err, db) {
			db.collection(collectionName).findAndModify(
				criteria,
				[],
				{ $set: { deleted: true } },
				{ new: true, fields: self.defaultProjection },
				this.slot()
			);
		},
		function(err, result) {
			this.pass(result);
			self.notify(result, self.RemoveAspect);
		},
		function(err, result) {
			this.pass(self.prepareRecord(result));
		},
		callback
	);
};

BaseModel.prototype.getId = function(callback) {
	console.assert(this.collectionName, 'Collection name must be specified');
	var collectionName = this.collectionName;
	this.getDb(function(err, db) {
		var collection = db.collection(sequencesCollection),
			self = this;
		collection.findAndModify({ collection: collectionName }, [], {
			$inc: {
				counter: 1
			}
		}, {
			new: true,
			upsert: true
		}, function(err, sequence) {
			callback(err, sequence.counter);
		});
	});
};

module.exports = BaseModel;
