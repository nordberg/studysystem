'use strict';

var BaseModel = require('./base'),
	_ = require('underscore'),
	extend = require('../utils/extend');

var GroupModel = function() {
	GroupModel.super.constructor.call(this, 'groups');
};

extend(GroupModel, BaseModel);

module.exports = new GroupModel();

