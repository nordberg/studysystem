'use strict';

var BaseModel = require('./base'),
	FileModel = require('./file'),
	_ = require('underscore'),
	fs = require('fs'),
	path = require('path'),
	uuid = require('node-uuid'),
	config = require('../config'),
	Steppy = require('twostep').Steppy,
	extend = require('../utils/extend');

var DocumentModel = function() {
	DocumentModel.super.constructor.call(this, 'documents');

	this.DeleteAspect = 'DocumentDeleteAspect';
	this.UpdateAspect = 'DocumentUpdateAspect';
};

extend(DocumentModel, BaseModel);

DocumentModel.prototype.insert = function(data, callback) {
	var self = this;
	Steppy(
		function() {
			var resObject = data;

			resObject.created = (new Date()).getTime();
			resObject.versions = [];

			this.pass(resObject);
		},
		function(err, data) {
			DocumentModel.super.insert.call(self, data, this.slot());
		},
		callback
	);
};

DocumentModel.prototype.createVersion = function(
	doc, content, author, callback
) {
	var self = this;
	Steppy(
		function() {
			FileModel.insert(content, this.slot());
		},
		function(err, fileName) {
			var updateObject = {};
			var versionToPush = {
				created: (new Date()).getTime(),
				name: fileName,
				author: author.id,
				state: 'undefined'
			};

			versionToPush.stateInfo = {
				date: versionToPush.created,
				user: versionToPush.author
			};

			updateObject.$push = {
				versions: versionToPush
			};

			this.pass(updateObject);
		},
		function(err, data) {
			DocumentModel.super.update.call(self, {
				id: doc
			}, data, this.slot());
		},
		function(err, data) {
			this.pass(data.versions.slice(-1)[0]);
		},
		callback
	);
};

DocumentModel.prototype.updateVersionState = function(
	docId, name, user, state, callback
) {
	var self = this;
	var versionProjection = {
		versions:  {
			'$elemMatch': {
				name: name
			}
		}
	};
	Steppy(
		function() {
			DocumentModel.super.findOne.call(
				self,
				{ id: docId, 'versions.name': name },
				versionProjection,
				this.slot()
			);
		},
		function(err, doc) {
			var version = doc.versions[0];
			if (!version) {
				return callback(null, version);
			}
			this.pass(version);

			DocumentModel.super.count.call(
				self,
				{ 'versions.author': version.author },
				this.slot()
			);
		},
		function(err, version, versionsCount) {
			var updateQuery = {
				$set: {
					'versions.$.state': state,
					'versions.$.stateInfo': {
						date: (new Date()).getTime(),
						user: user.id
					}
				}
			};

			if ((versionsCount === 1) && (state === 'rejected')) {
				updateQuery.$pull = {
					authors_ids: version.author
				};
			} else {
				updateQuery.$addToSet = {
					authors_ids: version.author
				};
			}

			DocumentModel.super.update.call(
				self,
				{ id: docId, 'versions.name': name },
				updateQuery,
				versionProjection,
				this.slot()
			);
		},
		function(err, updatedDoc) {
			this.pass(updatedDoc.versions[0]);
		},
		callback
	);
};

var getFileContent = function(err, doc) {
	if (doc && doc.versions && doc.versions[0]) {
		FileModel.findOne({	name: doc.versions[0].name }, this.slot());
	} else {
		this.pass(null);
	}
};

DocumentModel.prototype.loadContentFor = function(name, callback) {
	FileModel.findOne({	name: name }, callback);
};

DocumentModel.prototype.getContentFor = function(criteria, version, callback) {
	var self = this;
	Steppy(
		function() {
			DocumentModel.super.findOne.call(self, criteria, {
				versions: { $elemMatch: { name: version } }
			}, this.slot());
		},
		getFileContent,
		callback
	);
};

DocumentModel.prototype.getContentForActualVersion = function(criteria, callback) {
	var self = this;
	Steppy(
		function() {
			DocumentModel.super.findOne.call(self, criteria, {
				versions: { $slice: -1 }
			}, this.slot());
		},
		getFileContent,
		callback
	);
};

module.exports = new DocumentModel();

