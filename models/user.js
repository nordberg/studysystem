'use strict';

var BaseModel = require('./base'),
	Steppy = require('twostep').Steppy,
	GroupModel = require('../models').GroupModel,
	bcrypt = require('bcrypt'),
	_ = require('underscore'),
	ConstraintError = require('../utils/errors').ConstraintError,
	extend = require('../utils/extend');

var SALT_WORK_FACTOR = 10;

var UserModel = function() {
	UserModel.super.constructor.call(this, 'users');
};

extend(UserModel, BaseModel);

UserModel.prototype.createPassword = function(password, callback) {
	bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
		bcrypt.hash(password, salt, function(err, hash) {
			callback(err, hash);
		});
	});
};

UserModel.prototype.comparePassword = function(password, currentPassword, callback) {
	bcrypt.compare(password, currentPassword, callback);
};

UserModel.prototype.insert = function(data, callback) {
	var self = this;
	this.createPassword(data.password, function(err, hash) {
		if (err) {
			callback(err);
		} else {
			data.password = hash;
			UserModel.super.insert.call(self, data, callback);
		}
	});
};

UserModel.prototype.beforeUpdate = function(criteria, data, callback) {
	var self = this;
	if (data.groups_ids.length === 0) {
		UserModel.super.beforeUpdate.call(self, criteria, data, callback);
	} else {
		GroupModel.find({ id: { $in: data.groups_ids }}, function(err, groups) {
			if (err) return callback(err);

			var groupsHash = {};
			groups.forEach(function(group) {
				groupsHash[group.id] = group;
			});

			var result = data.groups_ids.every(function(id) {
				return groupsHash[id];
			});
			if (result) {
				UserModel.super.beforeUpdate.call(self, criteria, data, callback);
			} else {
				callback(new ConstraintError('Invalid Groups'));
			}
		});
	}
};

UserModel.prototype.update = function(criteria, data, callback) {
	var self = this;
	if (data.password) {
		this.createPassword(data.password, function(err, hash) {
			if (err) {
				callback(err);
			} else {
				data.password = hash;
				UserModel.super.update.call(self, criteria, data, callback);
			}
		});
	} else {
		UserModel.super.update.apply(self, arguments);
	}
};

module.exports = new UserModel();