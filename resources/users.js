'use strict';

var UserModel = require('../models').UserModel,
	WithValidationResource = require('./withValidation'),
	extend = require('../utils/extend'),
	schemas = require('../utils/defaultSchemas'),
	Steppy = require('twostep').Steppy;

var UsersResource = function() {
	UsersResource.super.constructor.call(this, UserModel, {
		availiableActions: {
			create: false,
			additionalAction: false
		}
	});

	this.pluralName = 'users';
	this.singleName = 'user';

	this.findOneSchema = schemas.findOneById;
	this.findSchema = schemas.find;
	this.updateSchema = {
		properties: {
			user: {
				type: 'object',
				required: true,
				properties: {
					id: schemas.requiredId,
					firstName: schemas.simpleString,
					lastName: schemas.simpleString,
					email: { type: 'email' },
					groups_ids: {
						type: 'array',
						uniqueItems: true,
						required: true,
						items: {
							type: 'integer',
							minimum: 0
						}
					}
				}
			},
			id: schemas.requiredId
		}
	};
};

extend(UsersResource, WithValidationResource);

UsersResource.prototype.getUri = function() {
	return [UsersResource.super.getUri.apply(this), 'users'].join('/');
};

var setupDefaultOutFields = function(req) {
	req.params.outFields = req.params.outFields || {};
	req.params.outFields.password = 0;
	req.params.outFields.email = 0;
};

UsersResource.prototype.beforeFind = function(err, req, res, callback) {
	if (err) return callback(err);

	setupDefaultOutFields(req);

	UsersResource.super.beforeFind.call(this, err, req, res, callback);
};

UsersResource.prototype.beforeFindOne = function(err, req, res, callback) {
	if (err) return callback(err);

	setupDefaultOutFields(req);

	UsersResource.super.beforeFindOne.call(this, err, req, res, callback);
};

UsersResource.prototype.beforeUpdate = function(err, req, res, callback) {
	if (err) return callback(err);

	if (req.user.isAdmin) {
		UsersResource.super.beforeUpdate.apply(this, arguments);
	} else {
		res.forbidden();
	}
};

UsersResource.prototype.beforeCreate = function(err, req, res, callback) {
	if (err) return callback(err);

	if (req.user.isAdmin) {
		UsersResource.super.beforeCreate.apply(this, arguments);
	} else {
		res.forbidden();
	}
};

module.exports = UsersResource;