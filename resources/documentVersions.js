'use strict';
var DocumentModel = require('../models').DocumentModel,
	AccessCheckService = require('../services').AccessCheckService,
	WithValidationResource = require('./withValidation'),
	schemas = require('../utils/defaultSchemas'),
	extend = require('../utils/extend'),
	_ = require('underscore'),
	Steppy = require('twostep').Steppy;

var DocumentVersionsResource = function() {
	DocumentVersionsResource.super.constructor.call(this, null, {
		availiableActions: {
			find: false,
			delete: false,
			update: false,
			additionalAction: true
		},
		key: 'name',
		keyPattern: '[a-z0-9-]+'
	});
	this.pluralName = 'versions';
	this.singleName = 'version';
	this.findOneSchema = {
		properties: {
			id: schemas.requiredId,
			name: schemas.requiredUUID
		}
	};
	this.createSchema = {
		properties: {
			id: schemas.requiredId,
			content: {
				type: 'string',
				required: true
			}
		}
	};
	this.additionalActionSchema = {
		properties: {
			id: schemas.requiredId,
			name: schemas.requiredUUID,
			action: {
				type: 'string',
				'enum': ['confirm', 'reject']
			}
		}
	};
};

extend(DocumentVersionsResource, WithValidationResource);

var userCanReadAll = function(user) {
	return _(user.groups).some(function(group) {
		return group.name == 'admin' || group.name == 'professor';
	});
};

DocumentVersionsResource.prototype.isVersionAvailable = function(version, user) {
	return version.state === 'confirmed' || version.author === user.id ||
		userCanReadAll(user);
};

DocumentVersionsResource.prototype.getUri = function() {
	return [DocumentVersionsResource.super.getUri.apply(this), 'versions'].join('/');
};

DocumentVersionsResource.prototype.findOne = function(err, req, res, callback) {
	var self = this;
	Steppy(
		function() {
			DocumentModel.findOne(
				{ id: req.params.id },
				{ versions: { $elemMatch: { name: req.params.name } } },
				this.slot()
			);
		},
		function(err, doc) {
			if (!doc || !doc.versions.length) {
				res.notFound();
			} else {
				if (self.isVersionAvailable(doc.versions[0], req.user)) {
					DocumentModel.loadContentFor(
						doc.versions[0].name, this.slot()
					);
				} else {
					res.forbidden();
				}
			}
		},
		function(err, content) {
			if (!content) {
				res.notFound();
			} else {
				this.pass(content);
			}
		},
		callback
	);
};

DocumentVersionsResource.prototype.create = function(err, req, res, callback) {
	Steppy(
		function(err, canConfirm) {
			DocumentModel.createVersion(
				req.params.id,
				req.params.content,
				req.user,
				this.slot()
			);
		},
		function(err, version) {
			this.pass(version);
			AccessCheckService.can(
				req.user, 'document', 'versions.confirm', this.slot()
			);
		},
		function(err, version, canConfirm) {
			if (canConfirm) {
				DocumentModel.updateVersionState(
					req.params.id,
					version.name,
					req.user,
					'confirmed',
					this.slot()
				);
			} else {
				this.pass(version);
			}
		},
		callback
	);
};

DocumentVersionsResource.prototype.additionalAction = function(err, req, res, callback) {
	if (req.params.action) {
		if (req.params.action == 'confirm') {
			Steppy(
				function() {
					AccessCheckService.can(
						req.user, 'document', 'versions.confirm', this.slot()
					);
				},
				function(err, canConfirm) {
					if (canConfirm) {
						DocumentModel.updateVersionState(
							req.params.id,
							req.params.name,
							req.user,
							'confirmed',
							this.slot()
						);
					} else {
						res.forbidden();
					}
				},
				callback
			);

		}
		if (req.params.action == 'reject') {
			Steppy(
				function() {
					AccessCheckService.can(
						req.user, 'document', 'versions.reject', this.slot()
					);
				},
				function(err, canReject) {
					if (canReject) {
						DocumentModel.updateVersionState(
							req.params.id,
							req.params.name,
							req.user,
							'rejected',
							this.slot()
						);
					} else {
						res.forbidden();
					}
				},
				callback
			);
		}
	}
};
module.exports = DocumentVersionsResource;