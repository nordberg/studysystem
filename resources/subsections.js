'use strict';

var SubsectionModel = require('../models').SubsectionModel,
	WithValidationResource = require('./withValidation'),
	schemas = require('../utils/defaultSchemas'),
	clone = require('clone'),
	extend = require('../utils/extend'),
	Steppy = require('twostep').Steppy;

var SubsectionsResource = function() {
	SubsectionsResource.super.constructor.call(this, SubsectionModel);
	this.pluralName = 'subsections';
	this.singleName = 'subsection';
	this.findSchema = {
		properties: {
			id: schemas.id,
			alias: schemas.simpleString,
			published: {
				type: 'boolean'
			}
		}
	};
	this.findOneSchema = {
		properties: {
			id: schemas.requiredId
		}
	};

	var sectionSchema = {
		properties: {
			subsection: {
				type: 'object',
				required: true,
				properties: {
					title: schemas.requiredSimpleString,
					description: schemas.simpleString,
					alias: schemas.requiredSimpleString,
					order: schemas.requiredId,
					published: {
						type: 'boolean',
						required: true,
						'default': false
					},
					type: {
						type: 'string',
						required: true,
						'enum': ['practice', 'lection', 'lab']
					},
					content: schemas.id
				}
			}
		}
	};
	this.additionalActionSchema = {
		properties: {
			id: schemas.requiredId,
			action: schemas.requiredSimpleString
		}
	};
	this.createSchema = sectionSchema;

	var updateSchema = clone(sectionSchema);
	updateSchema.properties.id = schemas.requiredId;

	this.updateSchema = updateSchema;
};

extend(SubsectionsResource, WithValidationResource);

SubsectionsResource.prototype.getUri = function() {
	return [SubsectionsResource.super.getUri.apply(this), 'subsections'].join('/');
};

SubsectionsResource.prototype.beforeFind = function(err, req, res, callback) {
	if (err) return callback(err);

	if (req.user && !req.user.isAdmin) {
		req.params.published = true;
	}

	SubsectionsResource.super.beforeFind.apply(this, arguments);
};

SubsectionsResource.prototype.beforeFindOne = function(err, req, res, callback) {
	if (err) return callback(err);

	if (!req.user.isAdmin) {
		req.params.published = true;
	}

	SubsectionsResource.super.beforeFindOne.apply(this, arguments);
};

SubsectionsResource.prototype.update =  function(err, req, res, callback) {
	var self = this;
	Steppy(
		function() {
			var params = {};
			params[self.key] = req.params[self.key];
			console.log(params);
			this.pass(params);
			self.resourceModel.findOne(params, this.slot());
		},
		function(err, params, resource) {
			if (!resource) {
				res.notFound();
			} else {
				if (req.user.isAdmin || resource.authors_ids.indexOf(req.user.id) != -1) {
					self.resourceModel.update(
						params,
						req.params[self.singleName],
						this.slot()
					);
				} else {
					res.forbidden();
				}
			}
		},
		callback
	);
};

SubsectionsResource.prototype.beforeCreate = function(err, req, res, callback) {
	if (err) return callback(err);

	if (req.user.isAdmin) {
		SubsectionsResource.super.beforeCreate.apply(this, arguments);
	} else {
		res.forbidden();
	}
};

module.exports = SubsectionsResource;

