'use strict';

var SectionModel = require('../models').SectionModel,
	WithValidationResource = require('./withValidation'),
	schemas = require('../utils/defaultSchemas'),
	clone = require('clone'),
	extend = require('../utils/extend'),
	Steppy = require('twostep').Steppy;

var SectionsResource = function() {
	SectionsResource.super.constructor.call(this, SectionModel);
	this.pluralName = 'sections';
	this.singleName = 'section';
	this.findSchema = schemas.find;
	this.findOneSchema = {
		properties: {
			id: schemas.requiredId
		}
	};

	var sectionSchema = {
		properties: {
			section: {
				type: 'object',
				required: true,
				properties: {
					id: schemas.requiredId,
					title: schemas.requiredSimpleString,
					description: schemas.simpleString,
					alias: schemas.requiredSimpleString,
					duration: schemas.requiredId,
					published: {
						type: 'boolean',
						required: true,
						'default': false
					},
					subsections_ids: {
						type: 'array',
						required: true,
						items: schemas.requiredId
					}
				}
			}
		}
	};
	// this.additionalActionSchema = {
	// 	properties: {
	// 		id: schemas.requiredId,
	// 		action: schemas.requiredSimpleString
	// 	}
	// };
	// this.createSchema = sectionSchema;
    //
	// var updateSchema = clone(sectionSchema);
	// updateSchema.properties.id = schemas.requiredId;
    //
	// this.updateSchema = updateSchema;
};

extend(SectionsResource, WithValidationResource);

SectionsResource.prototype.getUri = function() {
	return [SectionsResource.super.getUri.apply(this), 'sections'].join('/');
};

SectionsResource.prototype.beforeFind = function(err, req, res, callback) {
	if (err) return callback(err);

	// if (req.user && !req.user.isAdmin) {
	// 	req.params.published = true;
	// }

	SectionsResource.super.beforeFind.apply(this, arguments);
};
//
// SectionsResource.prototype.beforeFindOne = function(err, req, res, callback) {
// 	if (err) return callback(err);
//
// 	if (!req.user.isAdmin) {
// 		req.params.published = true;
// 	}
//
// 	SectionsResource.super.beforeFindOne.apply(this, arguments);
// };
//
// SectionsResource.prototype.update =  function(err, req, res, callback) {
// 	var self = this;
// 	Steppy(
// 		function() {
// 			var params = {};
// 			params[self.key] = req.params[self.key];
// 			console.log(params);
// 			this.pass(params);
// 			self.resourceModel.findOne(params, this.slot());
// 		},
// 		function(err, params, resource) {
// 			if (!resource) {
// 				res.notFound();
// 			} else {
// 				if (req.user.isAdmin || resource.authors_ids.indexOf(req.user.id) != -1) {
// 					self.resourceModel.update(
// 						params,
// 						req.params[self.singleName],
// 						this.slot()
// 					);
// 				} else {
// 					res.forbidden();
// 				}
// 			}
// 		},
// 		callback
// 	);
// };
//
// SectionsResource.prototype.beforeCreate = function(err, req, res, callback) {
// 	if (err) return callback(err);
//
// 	if (req.user.isAdmin) {
// 		SectionsResource.super.beforeCreate.apply(this, arguments);
// 	} else {
// 		res.forbidden();
// 	}
// };

module.exports = SectionsResource;
