'use strict';

var CoursesResource = require('./courses'),
	SectionsResource = require('./sections'),
	SubsectionsResource = require('./subsections'),
	UsersResource = require('./users'),
	DocumentsResource = require('./documents'),
	GroupsResource = require('./groups');

exports.CoursesResource = CoursesResource;
exports.SectionsResource = SectionsResource;
exports.UsersResource = UsersResource;
exports.GroupsResource = GroupsResource;
exports.DocumentsResource = DocumentsResource;

exports.register = function(app) {
	var coursesResource = new CoursesResource();
	coursesResource.register(app);

	var sectionsResource = new SectionsResource();
	sectionsResource.register(app);

	var subsectionsResource = new SubsectionsResource();
	subsectionsResource.register(app);

	var usersResource = new UsersResource();
	usersResource.register(app);

	var groupsResource = new GroupsResource();
	groupsResource.register(app);

	var documentsResource = new DocumentsResource();
	documentsResource.register(app);

	require('./auth')(app);
};
