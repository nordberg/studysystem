'use strict';

var UserModel = require('../models').UserModel,
	SessionModel = require('../models').SessionModel,
	Steppy = require('twostep').Steppy;

module.exports = function(app) {
	app.post('/login', function(req, res) {
		Steppy(
			function() {
				if (req.user && req.token) {
					res.ok({
						user: req.user.id,
						token: req.token
					});
				} else {
					UserModel.findOne({ email: req.body.email }, this.slot());
				}
			},
			function(err, user) {
				if (!user) {
					res.notFound('Invalid login or password');
				} else {
					this.pass(user);
					UserModel.comparePassword(
						req.body.password, user.password, this.slot()
					);
				}
			},
			function(err, user, isPasswordIdentical) {
				if (!isPasswordIdentical) {
					res.notFound('Invalid login or password');
				} else {
					this.pass(user);
					req.session.getBy({ userId: user.id }, this.slot());
				}
			},
			function(err, user, session) {
				this.pass(user);
				if (session) {
					req.session.remove(session.token, this.slot());
				}
			},
			function(err, user) {
				this.pass(user);
				req.session.create(this.slot());
			},
			function(err, user, session) {
				this.pass(user);
				req.session.set(session.token, {userId: user.id}, this.slot());
			},
			function(err, user, session) {
				res.ok({
					user: user.id,
					token: session.token
				});
			},
			res.onError
		);
	});

	app.post('/register', function(req, res) {
		Steppy(
			function() {
				if (req.user.isAdmin) {
					UserModel.findOne({
						email: req.body.user.email
					}, this.slot());
				} else {
					res.forbidden();
				}
			},
			function(err, user) {
				if (!user) {
					UserModel.insert(req.body.user, this.slot());
				} else {
					res.alreadyExist('User already exist');
				}
			},
			function(err, user) {
				res.ok({ user: user });
			},
			res.onError
		);
	});

	app.post('/logout', function(req, res) {
		Steppy(
			function() {
				if (req.user) {
					req.session.getBy({ userId: parseInt(req.user.id, 10)}, this.slot());
				} else {
					res.ok();
				}
			},
			function(err, session) {
				req.session.remove(session.token, this.slot());
			},
			function(err, result) {
				res.ok({});
			},
			res.onError
		);
	});
};
