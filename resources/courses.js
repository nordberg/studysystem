'use strict';

var CourseModel = require('../models').CourseModel,
	WithValidationResource = require('./withValidation'),
	schemas = require('../utils/defaultSchemas'),
	clone = require('clone'),
	extend = require('../utils/extend'),
	Steppy = require('twostep').Steppy;

var CoursesResource = function() {
	CoursesResource.super.constructor.call(this, CourseModel);
	this.pluralName = 'courses';
	this.singleName = 'course';
	this.findSchema = {
		properties: {
			id: schemas.id,
			alias: schemas.simpleString,
			published: {
				type: 'boolean'
			}
		}
	};
	this.findOneSchema = {
		properties: {
			id: schemas.requiredId
		}
	};

	var courseSchema = {
		properties: {
			course: {
				type: 'object',
				required: true,
				properties: {
					title: schemas.requiredSimpleString,
					description: schemas.requiredSimpleString,
					alias: schemas.requiredSimpleString,
					published: {
						type: 'boolean',
						required: true,
						'default': false
					},
					materials: {
						type: 'array',
						required: true,
						items: {
							type: 'object',
							properties: {
								title: schemas.requiredSimpleString,
								description: schemas.simpleString,
								link: {
									type: 'string',
									format: 'url'
								}
							}
						}
					},
					authors_ids: {
						type: 'array',
						required: true,
						items: schemas.requiredId
					},
					sections_ids: {
						type: 'array',
						required: true,
						items: schemas.requiredId
					}
				}
			}
		}
	};
	this.additionalActionSchema = {
		properties: {
			id: schemas.requiredId,
			action: schemas.requiredSimpleString
		}
	};
	this.createSchema = courseSchema;

	var updateSchema = clone(courseSchema);
	updateSchema.properties.id = schemas.requiredId;

	this.updateSchema = updateSchema;
};

extend(CoursesResource, WithValidationResource);

CoursesResource.prototype.getUri = function() {
	return [CoursesResource.super.getUri.apply(this), 'courses'].join('/');
};

CoursesResource.prototype.beforeFind = function(err, req, res, callback) {
	if (err) return callback(err);

	if (req.user && !req.user.isAdmin) {
		req.params.published = true;
	}

	CoursesResource.super.beforeFind.apply(this, arguments);
};

CoursesResource.prototype.beforeFindOne = function(err, req, res, callback) {
	if (err) return callback(err);

	if (!req.user.isAdmin) {
		req.params.published = true;
	}

	CoursesResource.super.beforeFindOne.apply(this, arguments);
};

CoursesResource.prototype.update =  function(err, req, res, callback) {
	var self = this;
	Steppy(
		function() {
			var params = {};
			params[self.key] = req.params[self.key];
			console.log(params)
			this.pass(params);
			self.resourceModel.findOne(params, this.slot());
		},
		function(err, params, resource) {
			if (!resource) {
				res.notFound();
			} else {
				if (req.user.isAdmin || resource.authors_ids.indexOf(req.user.id) != -1) {
					self.resourceModel.update(
						params,
						req.params[self.singleName],
						this.slot()
					);
				} else {
					res.forbidden();
				}
			}
		},
		callback
	);
};

CoursesResource.prototype.beforeCreate = function(err, req, res, callback) {
	if (err) return callback(err);

	if (req.user.isAdmin) {
		CoursesResource.super.beforeCreate.apply(this, arguments);
	} else {
		res.forbidden();
	}
};

module.exports = CoursesResource;
