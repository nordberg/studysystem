'use strict';

var DocumentModel = require('../models').DocumentModel,
	DocumentVersionsResource = require('./documentVersions'),
	documentVersionsResource = new DocumentVersionsResource(),
	AccessCheckService = require('../services').AccessCheckService,
	WithValidationResource = require('./withValidation'),
	schemas = require('../utils/defaultSchemas'),
	extend = require('../utils/extend'),
	_ = require('underscore'),
	Steppy = require('twostep').Steppy;

var DocumentsResource = function() {
	DocumentsResource.super.constructor.call(this, DocumentModel);
	this.pluralName = 'documents';
	this.singleName = 'document';
	this.findOneSchema = schemas.findOneById;
	this.findSchema = schemas.find;
	this.createSchema = {
		properties: {
			'document': {
				type: 'object',
				required: true,
				properties: {
					title: schemas.requiredSimpleString,
					description: schemas.simpleString,
					authors_ids: {
						type: 'array',
						minLength: 1,
						maxLength: 1,
						required: true,
						items: {
							type: 'integer',
							required: true
						}
					}
				}
			}
		}
	};
	this.updateSchema = {
		properties: {
			'document': {
				type: 'object',
				required: true,
				properties: {
					id: schemas.requiredId,
					title: schemas.simpleString,
					description: schemas.simpleString,
				}
			}
		}
	};
};

extend(DocumentsResource, WithValidationResource);

DocumentsResource.prototype.getUri = function() {
	return [DocumentsResource.super.getUri.apply(this), 'documents'].join('/');
};

var prepareVersions = function(versions, user) {
	return versions.filter(function(version) {
		return documentVersionsResource.isVersionAvailable(version, user);
	});
}

DocumentsResource.prototype.find = function(err, req, res, callback) {
	var self = this;
	Steppy(
		function(err, params) {
			var projection = req.params.outFields || {};
			delete req.params.outFields;

			DocumentModel.find(req.params, projection, this.slot());
		},
		function(err, docs) {
			docs = _(docs).map(function(doc) {
				doc.versions = prepareVersions(doc.versions, req.user);
				return doc;
			});

			this.pass(docs);
		},
		callback
	);
};

DocumentsResource.prototype.findOne = function(err, req, res, callback) {
	var self = this;
	Steppy(
		function(err, params) {
			var projection = req.params.outFields || {};
			var params = {};

			params[self.key] = req.params[self.key];

			DocumentModel.findOne(params, projection, this.slot());
		},
		function(err, doc) {
			if (doc) {
				doc.versions = prepareVersions(doc.versions, req.user);
			}
			this.pass(doc);
		},
		callback
	);
};

DocumentsResource.prototype.register = function(app) {
	DocumentsResource.super.register.apply(this, arguments);

	this.nest(documentVersionsResource, 'versions');

	documentVersionsResource.register(app);
};

DocumentsResource.prototype.beforeUpdate = function(err, req, res, callback) {
	if (err) return callback(err);

	if (req.user.isAdmin) {
		DocumentsResource.super.beforeUpdate.apply(this, arguments);
	} else {
		res.forbidden();
	}
};

DocumentsResource.prototype.beforeCreate = function(err, req, res, callback) {
	if (err) return callback(err);

	if (req.user.isAdmin) {
		DocumentsResource.super.beforeCreate.apply(this, arguments);
	} else {
		res.forbidden();
	}
};

module.exports = DocumentsResource;
