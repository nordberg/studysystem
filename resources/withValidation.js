'use strict';

var BaseResource = require('./base'),
	extend = require('../utils/extend'),
	conform = require('conform');

var WithValidationResource = function() {
	WithValidationResource.super.constructor.apply(this, arguments);
};

extend(WithValidationResource, BaseResource);

WithValidationResource.prototype.beforeEach = function(err, req, res, actionName, callback) {
	var schemaName = actionName + 'Schema',
		schema = this[schemaName];

	if (!schema) {
		return callback(
			new Error('Schema for action ' + actionName + ' is undefined')
		);
	}

	var result = conform.validate(req.params, schema);

	if (!result.valid) {
		return res.invalidParams(result.errors);
	}

	WithValidationResource.super.beforeEach.apply(this, arguments);
};

module.exports = WithValidationResource;