'use strict';

var _ = require('underscore'),
	_s = require('underscore.string'),
	conform = require('conform'),
	config = require('../config'),
	Steppy = require('twostep').Steppy;

var BaseResource = function(resourceModel, params) {
	this.resourceModel = resourceModel;

	params = params || {};
	params.availiableActions = params.availiableActions || {};

	this.availiableActions = _.defaults(params.availiableActions, {
		findOne: true,
		find: true,
		create: true,
		additionalAction: false,
		delete: false,
		update: true
	});

	this.key = params.key || 'id';
	this.keyPattern = params.keyPattern || '[0-9]+';
};

BaseResource.prototype._isResourceAction = function(actionName) {
	return [
		'findOne', 'delete', 'update', 'additionalAction'
	].indexOf(actionName) !== -1;
};

BaseResource.prototype._isSingleResponse = function(actionName) {
	return [
		'findOne', 'delete', 'create', 'update', 'additionalAction'
	].indexOf(actionName) !== -1;
};

BaseResource.prototype._buildUri = function(actionName) {
	if (this._isResourceAction(actionName)) {
		return this.getResourceUri();
	} else {
		return this.getUri();
	}
};

BaseResource.prototype.getUri = function() {
	return config.apiPrefix;
};

BaseResource.prototype.buildKey = function() {
	return this.key + '(' + this.keyPattern + ')';
};

BaseResource.prototype.getResourceUri = function() {
	return [this.getUri(), this.buildKey()].join('/:');
};

BaseResource.prototype.nest = function(resource, name) {
	var self = this;
	resource.getUri = function() {
		return [self.getResourceUri(), name].join('/');
	};
};

BaseResource.prototype.find = function(err, req, res, callback) {
	if (err) return callback(err);

	var projection = req.params.outFields || {};

	delete req.params.outFields;

	this.resourceModel.find(req.params, projection, callback);
};

BaseResource.prototype.findOne = function(err, req, res, callback) {
	if (err) return callback(err);

	var params = _.chain(req.params).omit('outFields').clone().value();

	var projection = req.params.outFields || {};

	this.resourceModel.findOne(params, projection, callback);
};

BaseResource.prototype.update =  function(err, req, res, callback) {
	var self = this;
	Steppy(
		function() {
			var params = {};
			params[self.key] = req.params[self.key];
			console.log(params)
			this.pass(params);
			self.resourceModel.findOne(params, this.slot());
		},
		function(err, params, resource) {
			if (!resource) {
				res.notFound();
			} else {
				self.resourceModel.update(
					params,
					req.params[self.singleName],
					this.slot()
				);
			}
		},
		callback
	);
};

BaseResource.prototype.create = function(err, req, res, callback) {
	this.resourceModel.insert(req.params[this.singleName], callback);
};

BaseResource.prototype.delete = function(err, req, res, callback) {
	var self = this;
	Steppy(
		function() {
			var params = {
				id: parseInt(req.params.id, 10)
			};
			this.pass(params);
			self.resourceModel.findOne(params, this.slot());
		},
		function(err, params, resource) {
			if (!resource) {
				res.notFound();
			} else {
				self.resourceModel.remove(params, this.slot());
			}
		},
		callback
	);
};

BaseResource.prototype.notAllowed = function(req, res) {
	res.notAllowed();
};

BaseResource.prototype.prepareParams = function(err, req, callback) {
	var resultParams = {};

	_(req.body).each(function(value, key) {
		resultParams[key] = value;
	});
	_(req.query).each(function(value, key) {
		resultParams[key] = value;
	});
	for (var key in req.params) {
		resultParams[key] = req.params[key];
	}

	req.params = resultParams;

	if (config.loqRequestParams) {
		console.log(JSON.stringify(req.params, null, 4));
	}

	callback(null);
};

BaseResource.prototype.beforeEach = function(err, req, res, action, callback) {
	if (err) return callback(err);

	req.checkAuthorize(null, callback);
};

BaseResource.prototype.beforeFindOne = function(err, req, res, callback) {
	callback(err, req.params);
};

BaseResource.prototype.beforeFind = function(err, req, res, callback) {
	callback(err, req.params);
};

BaseResource.prototype.beforeUpdate = function(err, req, res, callback) {
	callback(err, req.params);
};

BaseResource.prototype.beforeCreate = function(err, req, res, callback) {
	callback(err, req.params);
};

BaseResource.prototype.beforeDelete = function(err, req, res, callback) {
	callback(err, req.params);
};

BaseResource.prototype.beforeAdditionalAction = function(err, req, res, callback) {
	callback(err, req.params);
};

BaseResource.prototype.afterEach = function(err, req, res, result, action, callback) {
	if (err) return callback(err);

	if (result) {
		var responseData = {};
		if (this._isSingleResponse(action)) {
			responseData[this.singleName] = result;
		} else {
			responseData[this.pluralName] = result;
		}
		callback(null, responseData);
	} else {
		callback(err, result);
	}
};

BaseResource.prototype.register = function(app) {
	var actionsMethods = {
		findOne: 'get',
		find: 'get',
		create: 'post',
		additionalAction: 'post',
		delete: 'delete',
		update: 'put'
	};

	var self = this;

	_(this.availiableActions).each(function(avaliability, actionName) {
		var actionMethod = actionsMethods[actionName],
			action = app[actionMethod],
			uri = self._buildUri(actionName);

		if (avaliability && self[actionName]) {
			console.log('registering', actionMethod, uri);
			app[actionMethod](uri, function(req, res) {
				Steppy(
					function() {
						self.prepareParams(null, req, this.slot());
					},
					function() {
						self.beforeEach(null, req, res, actionName, this.slot());
					},
					function(err) {
						var before = self['before' + _s.capitalize(actionName)];
						if (before) {
							before.call(self, err, req, res, this.slot());
						} else {
							this.pass(req.params);
						}
					},
					function(err, params) {
						self[actionName](err, req, res, this.slot());
					},
					function(err, result) {
						self.afterEach(err, req, res, result, actionName, this.slot());
					},
					function(err, data) {
						if (data) {
							if (actionName == 'create') {
								res.created(data);
							} else {
								res.ok(data);
							}
						} else {
							res.notFound();
						}
					},
					res.onError
				);
			});
		} else {
			app[actionMethod](uri, function(req, res) {
				res.notAllowed();
			});
		}
	});
};

module.exports = BaseResource;
