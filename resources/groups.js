'use strict';

var GroupModel = require('../models').GroupModel,
	WithValidationResource = require('./withValidation'),
	extend = require('../utils/extend'),
	schemas = require('../utils/defaultSchemas'),
	Steppy = require('twostep').Steppy;

var GroupsResourse = function() {
	GroupsResourse.super.constructor.call(this, GroupModel);
	this.pluralName = 'groups';
	this.singleName = 'group';
	this.findSchema = {
		properties: {
			name: schemas.simpleString,
			id: schemas.id
		}
	};
	this.findOneSchema = schemas.findOneById;
	this.createSchema = {
		properties: {
			group: {
				type: 'object',
				required: true,
				properties: {
					name: schemas.requiredSimpleString,
					title: schemas.requiredSimpleString,
					description: {
						type: 'string',
						pattern: '^[^#%&*{}\\:<>?\/+]*$',
						'default': ''
					}
				}
			}
		}
	};
	this.updateSchema = {
		properties: {
			group: {
				type: 'object',
				required: true,
				properties: {
					id: schemas.requiredId,
					name: schemas.requiredSimpleString,
					title: schemas.requiredSimpleString,
					description: {
						type: 'string',
						pattern: '^[^#%&*{}\\:<>?\/+]*$',
						'default': ''
					}
				}
			},
			id: schemas.requiredId
		}
	};
};

extend(GroupsResourse, WithValidationResource);

GroupsResourse.prototype.getUri = function() {
	return [GroupsResourse.super.getUri.apply(this), 'groups'].join('/');
};


GroupsResourse.prototype.beforeUpdate = function(err, req, res, callback) {
	if (err) return callback(err);

	if (req.user.isAdmin) {
		GroupsResourse.super.beforeUpdate.apply(this, arguments);
	} else {
		res.forbidden();
	}
};

GroupsResourse.prototype.beforeCreate = function(err, req, res, callback) {
	if (err) return callback(err);

	if (req.user.isAdmin) {
		GroupsResourse.super.beforeCreate.apply(this, arguments);
	} else {
		res.forbidden();
	}
};

module.exports = GroupsResourse;