'use strict';
var path = require('path');

var config = {
	database: {},
	apiPrefix: '/api',
	documents: {
		path: path.join(__dirname, 'documents')
	},
	env: 'dev',
	loqRequestParams: false,
	testDumpURL: 'http://54.229.65.94/data.tar.gz',
	dbName: 'studysystem',
	testDbName: 'studysystem_test'
};

var env = process.env.NODE_ENV ? process.env.NODE_ENV.toLowerCase() : 'dev';
config.env = env;

if (env === 'test') {
	config.documents.path = '/tmp/documents_test';
	config.database.path = 'mongodb://127.0.0.1:27017/' + config.testDbName;
} else {
	config.database.path = 'mongodb://127.0.0.1:27017/' + config.dbName;
}

module.exports = config;