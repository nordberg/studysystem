'use strict';
var _ = require('underscore');

var commonProperties = {};

var outFields = {
	outFields: {
		type: 'object'
	}
};
module.exports.outFields = {
	type: 'object'
};
module.exports.requiredId = {
	type: 'integer',
	required: true,
	minimum: 0
};
module.exports.id = {
	type: 'integer',
	minimum: 0
};
module.exports.requiredUUID = {
	type: 'string',
	required: true,
	pattern: '[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}'
};
module.exports.simpleString = {
	type: 'string',
	pattern: '^[^#%&*{}\\:<>?\/+]*$'
};
module.exports.requiredSimpleString = {
	type: 'string',
	required: true,
	pattern: '^[^#%&*{}\\:<>?\/+]+$'
};

module.exports.findOneById = {
	properties: _(_(commonProperties).clone()).extend({
		id: module.exports.requiredId,
		outFields: module.exports.outFields
	})
};

module.exports.find = {
	properties: _(_(commonProperties).clone()).extend({
		id: module.exports.id,
		outFields: module.exports.outFields
	})
};
