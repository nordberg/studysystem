'use strict';

var extend = require('./extend');

var ConstraintError = function() {};

extend(ConstraintError, Error);

var NotFoundError = function() {};

extend(NotFoundError, Error);

module.exports = {
	ConstraintError: ConstraintError,
	NotFoundError: NotFoundError
};