'use strict';
var assert = require('assert');

var allAspects = 'AllASpects';

var Subject = function() {
	this.observers = {};
};

Subject.prototype._getObservers = function(aspect) {
	aspect = aspect || allAspects;
	if (this.observers) {
		return this.observers[aspect] || [];
	} else {
		return [];
	}
};

Subject.prototype.notify = function(data, aspect, callback) {
	// assert(this.observers, 'Observers is undefined');
	var observers = this._getObservers(aspect),
		count = observers.length - 1;

	var notifyCallback = function() {
		if (count === 0) {
			callback(null);
		} else {
			count--;
		}
	};

	observers.forEach(function(observer) {
		observer.inform(data, aspect, notifyCallback);
	});
};

Subject.prototype.attach = function(observer, aspect) {
	assert(observer, 'Observer should be defined');
	assert(observer.inform, 'Observer should have inform method');

	if (!this.observers) {
		this.observers = {};
	}
	aspect = aspect || allAspects;

	this.observers[aspect] = this.observers[aspect] || [];
	this.observers[aspect].push(observer);
};

Subject.prototype.detach = function(observer, aspect) {
	assert(this.observers, 'Observers is undefined');

	aspect = aspect || allAspects;

	this.observers[aspect] = this.observers[aspect] || [];
	this.observers[aspect] = this.observers[aspect].filter(function(o) {
		return o != observer;
	});
};

module.exports = Subject;