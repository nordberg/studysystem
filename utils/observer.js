'use strict';

var Observer = function() {

};

Observer.prototype.inform = function(data, event, callback) {
	throw new Error('Inform should be implemented');
};

module.exports = Observer;