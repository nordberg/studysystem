'use strict';
var UserModel = require('../models').UserModel,
	GroupModel = require('../models').GroupModel,
	_ = require('underscore'),
	Steppy = require('twostep').Steppy;

var AccessCheckService = function() {

};

AccessCheckService.prototype.ckeckDocumentPermissions = function(user, action, callback) {
	var result = true;
	switch (action) {
		case 'versions.confirm':
			result = _(user.groups).some(function(group) {
				return group.name == 'admin' || group.name == 'professor';
			});
			break;
		default:
			result = true;
	}
	callback(null, result);
};

AccessCheckService.prototype.checkPermissions = function(user, resource, action, callback) {
	switch (resource) {
		case 'document':
			return this.ckeckDocumentPermissions(user, action, callback);
		default:
			return callback(new Error('Unsupported resource to check permissions'), false);
	}
};

AccessCheckService.prototype.can = function(user, resource, action, callback) {
	var self = this;
	Steppy(
		function() {
			if (typeof user === 'number') {
				UserModel.findOne({ id: user }, this.slot());
			} else {
				this.pass(user);
			}
		},
		function(err, user) {
			self.checkPermissions(user, resource, action, this.slot())
		},
		function(err, result) {
			callback(err, result);
		}
	)
};

module.exports = new AccessCheckService();