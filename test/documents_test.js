'use strict';

var DocumentModel = require('../models').DocumentModel,
	UserModel = require('../models').UserModel,
	fs = require('fs'),
	_ = require('underscore'),
	path = require('path'),
	config = require('../config'),
	expect = require('expect.js'),
	fixtures = require('./fixtures'),
	Steppy = require('twostep').Steppy;

var FIXTURES = {
	documents: [{
		title: 'my title',
		description: 'my description',
		authors_ids: [1]
	}, {
		title: 'foo bar 1',
		description: 'oo bar 21341',
		authors_ids: [2]
	}],
	versions: [
		'new version content',
		'new version content 2'
	]
};

describe('DocumentModel', function() {
	before(function(done) {
		fs.mkdir(config.documents.path, function() {
			fixtures.setUp(done);
		});
	});

	after(function(done) {
		fs.readdir(config.documents.path, function(err, files) {
			files.forEach(function(file) {
				fs.unlinkSync(path.join(config.documents.path, file));
			});
			fs.rmdir(config.documents.path, function(err, res) {
				fixtures.tearDown(done);
			});
		});
	});

	describe('#create', function() {
		after(function(done) {
			fixtures.tearDown(done);
		});
		it('should create document with specified data', function(done) {
			Steppy(
				function() {
					DocumentModel.insert(FIXTURES.documents[0], this.slot());
				},
				function(err, result) {
					expect(result.id).to.eql(1);
					expect(result.created).to.be.a('number');
					expect(result.authors_ids).to.eql(FIXTURES.documents[0].authors_ids);
					expect(result.versions).to.be.a('array');

					this.pass(result);
				},
				done
			);
		});
	});

	describe('#update', function() {
		beforeEach(function(done) {
			fixtures.setUp(function() {
				DocumentModel.insert(FIXTURES.documents[0], done);
			});
		});
		after(function(done) {
			fixtures.tearDown(done);
		});
		it('should update document', function(done) {
			Steppy(
				function() {
					DocumentModel.update(
						{ id: 1 },
						_(FIXTURES.documents[1]).omit('authors_ids'),
						this.slot()
					);
				},
				function(err, doc) {
					expect(doc.authors_ids).to.eql(FIXTURES.documents[0].authors_ids);
					expect(doc.description).to.eql(FIXTURES.documents[1].description);
					expect(doc.versions).to.be.a('array');
					expect(doc.versions).to.have.length(0);
					this.pass(doc);
				},
				done
			);
		});
	});
	describe('versions', function() {
		before(function(done) {
			fixtures.setUp(function() {
				DocumentModel.insert(FIXTURES.documents[0], done);
			});
		});
		after(function(done) {
			fixtures.tearDown(done);
		});
		it('should create first version', function(done) {
			Steppy(
				function() {
					UserModel.findOne({ id: 2 }, this.slot());
				},
				function(err, author) {
					this.pass(author);
					DocumentModel.createVersion(
						1, FIXTURES.versions[0], author, true, this.slot()
					);
				},
				function(err, author, version) {
					expect(version.confirmed).to.be(true);
					this.pass(author);
					this.pass(version);
					fs.readFile(
						path.join(config.documents.path, version.name),
						{ encoding: 'utf8' },
						this.slot()
					);
				},
				function(err, author, version, content) {
					this.pass(author);
					this.pass(version);

					expect(FIXTURES.versions[0]).to.eql(content);
					DocumentModel.find({}, this.slot());
				},
				function(err, author, version, documents) {
					expect(documents).to.have.length(1);
					var doc = documents[0];

					expect(doc.authors_ids).to.have.length(2);
					expect(doc.authors_ids).to.eql([
						FIXTURES.documents[0].authors_ids[0], author.id
					]);

					expect(doc.versions).to.have.length(1);
					expect(doc.versions[0].author).to.eql(author.id);
					expect(doc.versions[0]).to.eql(version);

					this.pass(true);
				},
				done
			);
		});
		it('should create second version wihthout authors_ids', function(done) {
			var versionAuthor = 3;
			Steppy(
				function() {
					UserModel.findOne({ id: versionAuthor }, this.slot());
				},
				function(err, author) {
					this.pass(author);
					DocumentModel.createVersion(
						1, FIXTURES.versions[1], author, false, this.slot()
					);
				},
				function(err, author, version) {
					expect(version.confirmed).to.be(false);
					this.pass(author);
					this.pass(version);
					fs.readFile(
						path.join(config.documents.path, version.name),
						{ encoding: 'utf8' },
						this.slot()
					);
				},
				function(err, author, version, content) {
					this.pass(author);
					this.pass(version);

					expect(FIXTURES.versions[1]).to.eql(content);
					DocumentModel.find({}, this.slot());
				},
				function(err, author, version, documents) {
					expect(documents).to.have.length(1);
					var doc = documents[0];

					expect(doc.authors_ids).to.have.length(2);
					expect(doc.authors_ids).to.eql([
						FIXTURES.documents[0].authors_ids[0],
						FIXTURES.documents[1].authors_ids[0]
					]);

					expect(doc.versions).to.have.length(2);
					expect(doc.versions[1].author).to.eql(author.id);
					expect(doc.versions[1]).to.eql(version);

					this.pass(true);
				},
				done
			);
		});
		it('should confirm version', function(done) {
			Steppy(
				function() {
					UserModel.findOne({ id : 1 }, this.slot());
				},
				function(err, user) {
					this.pass(user);
					DocumentModel.findOne({ id: 1 }, this.slot());
				},
				function(err, user, doc) {
					this.pass(user);
					var version = doc.versions.slice(-1)[0];
					DocumentModel.confirmVersion(
						doc.id, version.name, user, this.slot()
					);
				},
				function(err, user, version) {
					expect(version.confirmed).to.eql(true);
					expect(version.confirmationInfo).to.not.be.empty();
					expect(version.confirmationInfo.date).to.be.ok();
					expect(version.confirmationInfo.user).to.eql(user.id);
					this.pass(version);
				},
				function(err, version) {
					this.pass(version);
					DocumentModel.findOne({ id: 1 }, this.slot());
				},
				function(err, version, doc) {
					expect(doc.authors_ids).to.have.length(3);
					expect(doc.authors_ids.indexOf(version.author)).to.not.eql(-1);
					this.pass(true);
				},
				done
			);
		});
	});

	describe('#getContentFor', function() {
		var createdVersions =[];

		before(function(done) {
			Steppy(
				function() {
					fixtures.setUp(this.slot());
				},
				function(err) {
					DocumentModel.insert(FIXTURES.documents[0], this.slot());
				},
				function(err, doc) {
					this.pass(doc);
					UserModel.findOne({ id : 1 }, this.slot());
					UserModel.findOne({ id : 3 }, this.slot());
				},
				function(err, doc, admin, student) {
					DocumentModel.createVersion(
						doc.id, FIXTURES.versions[0], admin, true, this.slot()
					);
					DocumentModel.createVersion(
						doc.id, FIXTURES.versions[1], admin, true, this.slot()
					);
					DocumentModel.createVersion(
						doc.id, FIXTURES.versions[0], student, false, this.slot()
					);
				},
				function(err, ver1, ver2, ver3) {
					createdVersions.push(ver1, ver2, ver3);
					this.pass(true);
				},
				done
			);
		});
		after(function(done) {
			fixtures.tearDown(done);
		});

		it('should return content of first version', function(done) {
			Steppy(
				function() {
					DocumentModel.getContentFor(
						{ id: 1 }, createdVersions[0].name, this.slot()
					);
				},
				function(err, content) {
					expect(content.raw).to.eql(FIXTURES.versions[0]);
					this.pass(true);
				},
				done
			);
		});
		it('should return content of second version', function(done) {
			Steppy(
				function() {
					DocumentModel.getContentFor(
						{ id: 1 }, createdVersions[1].name, this.slot()
					);
				},
				function(err, content) {
					expect(content.raw).to.eql(FIXTURES.versions[1]);
					this.pass(true);
				},
				done
			);
		});
		it('should return empty cotent', function(done) {
			Steppy(
				function() {
					DocumentModel.getContentFor(
						{ id: 2 }, createdVersions[1].name, this.slot()
					);
				},
				function(err, content) {
					expect(content).to.be.eql(null);
					this.pass(true);
				},
				done
			);
		});
	});

	// describe('#getActualVersion', function() {
	// 	it('should return content of last version', function(done) {
	// 		Steppy(
	// 			function() {
	// 				DocumentModel.getContentForActualVersion({ id: 1 }, this.slot());
	// 			},
	// 			function(err, content) {
	// 				expect(content.raw).to.eql(FIXTURES.versions[1]);
	// 				this.pass(true);
	// 			},
	// 			done
	// 		);
	// 	});
	// 	it('should return empty content', function(done) {
	// 		Steppy(
	// 			function() {
	// 				DocumentModel.getContentForActualVersion({ id: 2 }, this.slot());
	// 			},
	// 			function(err, content) {
	// 				expect(content).to.be.eql(null);
	// 				this.pass(true);
	// 			},
	// 			done
	// 		);
	// 	});
	// });
});