'use strict';

var expect = require('expect.js'),
	BaseModel = require('../models').BaseModel,
	extend = require('../utils/extend'),
	Subject = require('../utils/subject'),
	fixtures = require('./fixtures'),
	_ = require('underscore'),
	Steppy = require('twostep').Steppy;

describe('BaseModel', function() {
	var TestModel = function() {
		TestModel.super.constructor.call(this, 'test_model');

		this.RemoveAspect = 'TestModelRemoveAspect';
	};

	extend(TestModel, BaseModel);
	_(TestModel.prototype).extend(Subject.prototype);

	var testModel = new TestModel();

	var docsToInsert = [
		{'foo': 'bar', 'foo1': '1'},
		{'foo': 'baz', 'foo1': '2'}
	];
	before(function(done) {
		Steppy(
			function() {
				var self = this;
				docsToInsert.forEach(function(doc) {
					testModel.insert(doc, self.slot());
				});
			},
			done
		);
	});
	after(function(done) {
		fixtures.tearDown(done);
	});
	describe('#remove', function() {
		it('should remove document', function(done) {
			Steppy(
				function() {
					testModel.remove({ id: 1 }, this.slot());
				},
				function(err, doc) {
					testModel.findOne({ id: 1 }, this.slot());
				},
				function(err, result) {
					expect(result).to.eql(null);
					this.pass(true);
				},
				function(err) {
					testModel.findOne({ id: 1, deleted: true }, this.slot());
				},
				function(err, deletedDoc) {
					expect(deletedDoc).to.be.ok();
					this.pass(true);
				},
				done
			);
		});
	});
});