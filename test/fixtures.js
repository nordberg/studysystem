'use strict';

var MongoClient = require('mongodb').MongoClient,
	config = require('../config'),
	Steppy = require('twostep').Steppy;

var connect;
var getConnect = function(callback) {
	if (connect) {
		return connect;
	} else {
		MongoClient.connect(config.database.path, callback);
	}
};

exports.setUp = function(callback) {
	Steppy(
		function() {
			getConnect(this.slot());
		},
		function(err, connection) {
			var groups = connection.collection('groups');
			groups.insert([{
				id: 1,
				name: 'admin',
				title: 'Администратор',
				description: 'Lorem ipsum dolor sit',
				deleted: false
			}, {
				id: 2,
				name: 'user',
				title: 'Пользователь',
				description: 'Lorem ipsum dolor sit amet',
				deleted: false
			}, {
				id: 3,
				name: 'student',
				title: 'Студент',
				description: 'Lorem ipsum dolor sit amet',
				deleted: false
			}], this.slot());
			var users = connection.collection('users');
			users.insert([{
				id: 1,
				firstName: 'Николай',
				lastName: 'Кузнецов',
				email: 's@example.com',
				password: '1234',
				groups: [{
					id: 1, name: 'admin', title: 'Администратор',
				}, {
					id: 2, name: 'user', title: 'Пользователь',
				}],
				deleted: false
			}, {
				id: 2,
				firstName: 'Николай1',
				lastName: 'Кузнецов1',
				email: 's1@example.com',
				password: '1234',
				groups: [{
					id: 1, name: 'admin', title: 'Администратор',
				}, {
					id: 3, name: 'student', title: 'Студент',
				}],
				deleted: false
			}, {
				id: 3,
				firstName: 'Николай1',
				lastName: 'Кузнецов1',
				email: 's1@example.com',
				password: '1234',
				groups: [{
					id: 3, name: 'student', title: 'Студент',
				}],
				deleted: false
			}, {
				id: 4,
				firstName: 'Николай1',
				lastName: 'Кузнецов1',
				email: 's1@example.com',
				password: '1234',
				groups: [{
					id: 3, name: 'student', title: 'Студент',
				}],
				deleted: false
			}], this.slot());
		},
		callback
	);
};

exports.tearDown = function(callback) {
	Steppy(
		function() {
			getConnect(this.slot());
		},
		function(err, connection) {
			this.pass(connection);
			connection.dropDatabase(this.slot());
		},
		function(err, connection) {
			connection.close(this.slot());
		},
		callback
	);
};
