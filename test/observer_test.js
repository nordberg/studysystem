'use strict';
var Observer = require('../utils/observer'),
	expect = require('expect.js'),
	extend = require('../utils/extend'),
	Subject = require('../utils/subject');

describe('Subject', function(){
	describe('#attach()', function(){
		var Observable, DummyObserver;

		beforeEach(function(){
			Observable = function() { };
			DummyObserver = function() {};

			extend(Observable, Subject);
			extend(DummyObserver, Observer);
		});

		it('should attach observer', function() {
			var subject = new Observable(),
				observer = new DummyObserver(),
				observer1 = new DummyObserver();

			subject.attach(observer);
			subject.attach(observer1);

			expect(subject._getObservers()).to.have.length(2);
			expect(subject._getObservers()[0]).to.equal(observer);
			expect(subject._getObservers()[1]).to.equal(observer1);
		});

		it('should throw error when attaching bad observer', function() {
			var subject = new Observable();
			expect(subject.attach).withArgs({}).to.throwError();
		});

		it('should attach observer with aspect', function() {
			var subject = new Observable(),
				observer = new DummyObserver(),
				observer1 = new DummyObserver();

			subject.attach(observer, 'SubjectUpdate');
			subject.attach(observer1, 'SubjectCreate');

			expect(subject._getObservers('SubjectUpdate')).to.have.length(1);
			expect(subject._getObservers('SubjectUpdate')[0]).to.equal(observer);

			expect(subject._getObservers('SubjectCreate')).to.have.length(1);
			expect(subject._getObservers('SubjectCreate')[0]).to.equal(observer1);
			expect(subject._getObservers()).to.have.length(0);
		});
	});

	describe('#inform()', function() {
		var Observable = function() { };
		var DummyObserver = function() {};

		beforeEach(function() {
			Observable = function() { };
			DummyObserver = function() {};

			extend(Observable, Subject);
			extend(DummyObserver, Observer);
		});

		it('should inform all observers', function() {
			var notifyCount = 0;

			DummyObserver.prototype.inform = function(data, aspect) {
				expect(aspect).to.be.equal(undefined);
				expect(data).to.eql({'foo': 1});
				notifyCount++;
			};

			var subject = new Observable(),
				observer = new DummyObserver(),
				observer1 = new DummyObserver();

			subject.attach(observer);
			subject.attach(observer1);

			subject.notify({'foo': 1});
			expect(notifyCount).to.eql(2);
		});

		it('should inform by aspect', function() {
			var notifyCount = 0,
				currentAspect;

			DummyObserver.prototype.inform = function(data, aspect) {
				expect(aspect).to.be.equal(currentAspect);
				expect(data).to.eql({'foo': 1});
				notifyCount++;
			};

			var subject = new Observable(),
				observer = new DummyObserver(),
				observer1 = new DummyObserver();

			subject.attach(observer, 'SubjectUpdate');
			subject.attach(observer1, 'SubjectCreate');

			currentAspect = 'SubjectUpdate';
			subject.notify({'foo': 1}, currentAspect);
			expect(notifyCount).to.eql(1);

			currentAspect = 'SubjectCreate';
			subject.notify({'foo': 1}, currentAspect);
			expect(notifyCount).to.eql(2);
		});
	});
});